!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2011 SAAO, Cape Town
!.IDENT    : RSS_Gaps_cor.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Gaps_cor
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Additional correction of Gaps for RSS spectra
!      Only for OBJECTS and FLATS
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Gaps_cor
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_Gaps_cor.prg,v 1.1 2019/06/22 13:47:36 akniazev Exp akniazev $
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
    DEFINE/PAR P1 ? C "Enter_Catalogue_File_Name:"
!
    DEFINE/LOCAL J/I/1/2 0,0
    DEFINE/LOCAL CATAL/I/1/1  0
    DEFINE/LOCAL INPUT/C/1/80 "" ALL
    define/local oname/c/1/60 "IMAGE_TMP"
!    DEFINE/LOCAL OUTPUT/C/1/80 "" ALL
    DEFINE/LOCAL REF/C/1/4 "    "
    DEFINE/LOCAL REF_SP/C/1/80 "" ALL
    DEFINE/LOCAL INCAT/C/1/20 "                    "
    DEFINE/LOCAL II/I/1/2 0,0
    DEFINE/LOCAL I/I/1/2 0,0
    CLEAR/ICAT
	!
	!...test, if really catalog name was entered...
	!
    write/out "*********************************"
    write/out "******** GAPS correction ********"
    write/out "*********************************"
	    !
    SHOW/ICAT {P1}
    II(2) = {OUTPUTI(1)}
    II(1) = M$INDEX(P1,".cat")
    IF II(1) .LE. 0 THEN
       WRITE/KEY INCAT {P1}.cat
    ELSE
       WRITE/KEY INCAT {P1}
    ENDIF
	!
	!...Main loop
	!
    J(1) = 0
LOOP:
    STORE/FRAME INPUT {INCAT}
    J(1) = {J(1)} + 1
    write/out "***** {INPUT}: *****"

    if "{{INPUT},CCDTYPE}" .eq. "OBJECT" .or. "{{INPUT},CCDTYPE}" .eq. "FLAT" then
!    if "{{INPUT},CCDTYPE}" .eq. "OBJECT" .or. "{{INPUT},CCDTYPE}" .eq. "ARC" then
	@@ RSS_Gaps {INPUT}
    endif

    IF CATAL .LT. II(2) GOTO LOOP
END:
    -delete {oname}.bdf
