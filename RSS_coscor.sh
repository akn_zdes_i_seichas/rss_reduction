#!/bin/bash -x
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#.COPYRIGHT: (c) 2020 SAAO, Cape Town
#.IDENT    : RSS_reduction.prg
#.AUTHOR   : A.Y. Kniazev
#.CALL     : @@ RSS_reduction
#.CALL FROM:
#.KEYWORDS :
#.OUTPUT   :
#.PURPOSE  :
#      SALT semiautomatic data reduction.
#
#.COMMENTS :
#.USAGE    :
#      Execute as:
# @@ RSS_reduction Night_name Spectral_program_name
# where:
#
#-------------------------------------------------------------------------
#
sed 's/ee.fits/eews.fits/g' < obj1.lst >obj1s.lst
sed 's/ee.fits/eewsf.fits/g' < obj1.lst >obj1f.lst
for i in `cat obj1s.lst`
do
    oi=`echo ${i}|sed 's/.fits/c.fits/g'`
    mask=`echo cosm_${i}|sed 's/eews.fits/eew.fits/g'`
    if [ -f ${i} -a -f ${mask} ]; then
        inmidas 55 -p -j "@@ RSS_coscor ${i} ${mask} ${oi}; wait 2; bye"
        mv ${oi} ${i}
    fi
done
for i in `cat obj1f.lst`
do
    oi=`echo ${i}|sed 's/.fits/c.fits/g'`
    mask=`echo cosm_${i}|sed 's/eewsf.fits/eew.fits/g'`
    if [ -f ${i} -a -f ${mask} ]; then
        inmidas 55 -p -j "@@ RSS_coscor ${i} ${mask} ${oi}; wait 2; bye"
        mv ${oi} ${i}
    fi
done
