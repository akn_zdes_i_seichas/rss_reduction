!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2011 SAAO, Cape Town
!.IDENT    : RSS_Bad_cor.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Bad_cor
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Additional correction for bad pixels in RSS spectra
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Bad_cor
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_Bad_cor.prg,v 1.1 2019/06/22 20:25:08 akniazev Exp akniazev $
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
    DEFINE/PAR P1 ? C "Enter_Catalogue_File_Name:"
!
    DEFINE/LOCAL J/I/1/2 0,0
    DEFINE/LOCAL CATAL/I/1/1  0
    DEFINE/LOCAL INPUT/C/1/80 "" ALL
    define/local oname/c/1/60 "IMAGE_TMP"
!    DEFINE/LOCAL OUTPUT/C/1/80 "" ALL
    DEFINE/LOCAL REF/C/1/4 "    "
    DEFINE/LOCAL REF_SP/C/1/80 "" ALL
    DEFINE/LOCAL INCAT/C/1/20 "                    "
    DEFINE/LOCAL II/I/1/2 0,0
    DEFINE/LOCAL I/I/1/2 0,0
    define/local dateobs/c/1/10 "1111-11-11"
    CLEAR/ICAT
	!
	!...test, if really catalog name was entered...
	!
    write/out "*********************************"
    write/out "***** Bad pixels correction *****"
    write/out "*********************************"
	    !
    SHOW/ICAT {P1}
    II(2) = {OUTPUTI(1)}
    II(1) = M$INDEX(P1,".cat")
    IF II(1) .LE. 0 THEN
       WRITE/KEY INCAT {P1}.cat
    ELSE
       WRITE/KEY INCAT {P1}
    ENDIF
	!
	!...Main loop
	!
    J(1) = 0
LOOP:
    STORE/FRAME INPUT {INCAT}
!    READ/KEY INPUT
!    WRITE/OUT input frame {INPUT}, input cat {INCAT}
!           ...
    J(1) = {J(1)} + 1
    write/out "***** {INPUT}: *****"
    dateobs = "{input(7:14)}"
    inputr(1) = {dateobs} - 20200101
    inputr(2) = {dateobs} - 20141101  !...before 28.06.2015
    inputr(3) = {dateobs} - 20120601
    

    write/out "***** {INPUT}: {{INPUT},GAINSET}+{{INPUT},ROSPEED} *****"
    if "{{INPUT},GAINSET}" .EQ. "BRIGHT" .AND. "{{INPUT},ROSPEED}" .EQ. "FAST" then
        if "{{INPUT},CCDSUM}" .EQ. "2 2" then
            write/out "***** {INPUT}: {{INPUT},CCDSUM} *****"
            comp/col {INPUT}.C458 = (C457+C459)/2
        endif
    endif

    if "{{INPUT},GAINSET}" .EQ. "FAINT" .AND. "{{INPUT},ROSPEED}" .EQ. "SLOW" then
        if "{{INPUT},CCDSUM}" .EQ. "2 2"  .or. "{{INPUT},CCDSUM}" .EQ. "2 4" then
            write/out "***** {INPUT}: {{INPUT},CCDSUM}: {inputr(1)} - {inputr(2)} - {inputr(3)} *****"
            if {inputr(1)} .ge. 0 then
!.................For 2020
                comp/col {INPUT}.C459 = (C458+C463)/2
                comp/col {INPUT}.C460 = (C459+C463)/2
                comp/col {INPUT}.C461 = (C460+C463)/2
                comp/col {INPUT}.C462 = (C461+C463)/2
!.........................
                comp/col {INPUT}.C2565 = (C2564+C2566)/2
            elseif {inputr(1)} .le. 0 .and. {inputr(2)} .gt. 0 then
!.......................For 2015-2019
                comp/col {INPUT}.C460 = (C459+C464)/2
                comp/col {INPUT}.C461 = (C460+C464)/2
                comp/col {INPUT}.C462 = (C461+C464)/2
                comp/col {INPUT}.C463 = (C462+C464)/2
!
!                comp/col {INPUT}.C455 = (C454+C464)/2
!                comp/col {INPUT}.C456 = (C455+C464)/2
!                comp/col {INPUT}.C457 = (C456+C464)/2
!                comp/col {INPUT}.C458 = (C457+C464)/2
!                comp/col {INPUT}.C459 = (C458+C464)/2
!                comp/col {INPUT}.C460 = (C459+C464)/2
!                comp/col {INPUT}.C461 = (C460+C464)/2
!                comp/col {INPUT}.C462 = (C461+C464)/2
!                comp/col {INPUT}.C463 = (C462+C464)/2
                !..................................
!                comp/col {INPUT}.C504 = (C503+C510)/2
!                comp/col {INPUT}.C505 = (C504+C510)/2
!                comp/col {INPUT}.C506 = (C505+C510)/2
!                comp/col {INPUT}.C507 = (C506+C510)/2
!                comp/col {INPUT}.C508 = (C507+C510)/2
!                comp/col {INPUT}.C509 = (C508+C510)/2
            elseif {inputr(3)} .ge. 0 then
!.......................Before 2012
                comp/col {INPUT}.C455 = (C454+C464)/2
                comp/col {INPUT}.C456 = (C455+C464)/2
                comp/col {INPUT}.C457 = (C456+C464)/2
                comp/col {INPUT}.C458 = (C457+C464)/2
                comp/col {INPUT}.C459 = (C458+C464)/2
                comp/col {INPUT}.C460 = (C459+C464)/2
                comp/col {INPUT}.C461 = (C460+C464)/2
                comp/col {INPUT}.C462 = (C461+C464)/2
                comp/col {INPUT}.C463 = (C462+C464)/2
!..................................
                comp/col {INPUT}.C504 = (C503+C510)/2
                comp/col {INPUT}.C505 = (C504+C510)/2
                comp/col {INPUT}.C506 = (C505+C510)/2
                comp/col {INPUT}.C507 = (C506+C510)/2
                comp/col {INPUT}.C508 = (C507+C510)/2
                comp/col {INPUT}.C509 = (C508+C510)/2
            endif
        endif
    endif

    IF CATAL .LT. II(2) GOTO LOOP
END:
    -delete {oname}.bdf
