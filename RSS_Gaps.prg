!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2011 SAAO, Cape Town
!.IDENT    : RSS_gaps.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : RSS_gaps.prg
!.CALL FROM:
!.KEYWORDS : Spectra, CCD gaps
!.OUTPUT   :
!.PURPOSE  :
!      To fill CCD gaps with something reasonable.
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_gaps input output
! where:
!
!
!.VERSION  :  /home/akniazev/midwork/RCS/Spec_ext.prg,v 1.1 2009/07/04 1
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
    define/par P1 ?             I   "Name for the input spectrum: "
	!
	!...Lets define our variables
	!
    define/local  iname/c/1/60  "{p1}"
    define/local  i/i/1/1        0
    define/local  date/r/1/1    11111111.0
    define/local  dateobs/c/1/10 "1111-11-11"
    define/local  r/r/1/1       1.
	!
	!...main body of program
	!
	    !...before 28.06.2015 it was certain
	    !
    dateobs = "{iname(7:14)}"
    inputr(1) = {dateobs} - 20141101  !...before 28.06.2015
    inputr(2) = {dateobs} - 20121231
     write/out "##### GAPs CORRECTION ##### "

	    !...For the 2x2 binning
	    !
    if "{{iname},CCDSUM}" .EQ. "2 2" .or.  "{{iname},CCDSUM}" .EQ. "2 4" then
        write/out "##### BIN:{{iname},CCDSUM}..{inputr(1)}..{inputr(2)} "
            !
            !...First gap
            !
        if {inputr(1)} .gt. 0 then
            extra/ima GAP1 = {iname} [@1017,<:@1076,>]    !- the current one (2016)
                !
            do i = 2 59 1
               r = ({i}-1)/59
               comp/col  GAP1.c{i} = c1 + {r}*(c60-c1)
            enddo
                !
        elseif {inputr(1)} .le. 0 .and. {inputr(2)} .gt. 0 then
            extra/ima GAP1 = {iname} [@1021,<:@1079,>]    !- before 28.06.2015
                !
            do i = 2 58 1
                comp/col  GAP1.c{i} = GAP1.c1 + ({i}-1)*(GAP1.c59-GAP1.c1)/58
            enddo
                !
        else
            write/out "***** Before 20120601 ..."
            extra/ima GAP1 = {iname} [@1015,<:@1080,>]     !- before 06.2012
                !
            do i = 2 65 1                                !- before 06.2012
                comp/col  GAP1.c{i} = GAP1.c1 + ({i}-1)*(GAP1.c66-GAP1.c1)/64
            enddo
                !
        endif
        insert/ima GAP1 {iname}
            !
            !...Second gap
            !
        if {inputr(1)} .gt. 0 then
            extra/ima GAP2 = {iname} [2095,<:2147,>]    !- current
                !
            do i = 2 52 1
                comp/col  GAP2.c{i} = GAP2.c1 + ({i}-1)*(GAP2.c53-GAP2.c1)/53
            enddo
                !
        elseif {inputr(1)} .le. 0 .and. {inputr(2)} .gt. 0 then
            extra/ima GAP2 = {iname} [2097,<:2149,>]     ! - before 28.06.2015
                !
            do i = 2 52 1
                comp/col  GAP2.c{i} = GAP2.c1 + ({i}-1)*(GAP2.c53-GAP2.c1)/53
            enddo
                !
        else
            write/out "***** Before 20120601 ..."
            extra/ima GAP2 = {iname} [2090,<:2150,>]     !- before 06.2012
                !
            do i = 2 60 1                              !- before 06.2012
                comp/col  GAP2.c{i} = GAP2.c1 + ({i}-1)*(GAP2.c61-GAP2.c1)/59
            enddo
                !
        endif
        insert/ima GAP2 {iname}
    endif
	    !
	    !...For the 4x4 binning
	    !
    if "{{iname},CCDSUM}" .EQ. "4 4" then
            !
            !...First gap
            !
        extra/ima GAP1 = {iname} [505,<:539,>]
            !
        do i = 2 34 1
            comp/col  GAP1.c{i} = GAP1.c1 + ({i}-1)*(GAP1.c35-GAP1.c1)/33
        enddo
        insert/ima GAP1 {iname}
            !
            !...Second gap
            !
         extra/ima GAP2 = {iname} [1046,<:1076,>]
             !
         do i = 2 30 1
             comp/col  GAP2.c{i} = GAP2.c1 + ({i}-1)*(GAP2.c31-GAP2.c1)/29
         enddo
         insert/ima GAP2 {iname}
    endif
	    !
	    !...For the 1x1 binning
	    !
    if "{{iname},CCDSUM}" .EQ. "1 1" then
	!
	!...Nothing
	!
	write/out " We are doing nothing ..."
    endif
	!
    -delete GAP1.bdf GAP2.bdf
