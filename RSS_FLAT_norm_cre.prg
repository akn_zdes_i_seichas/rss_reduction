!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2013 SAAO, Cape Town
!.IDENT    : RSS_FLAT_norm_cre.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : RSS_FLAT_norm_cre.prg
!.CALL FROM:
!.KEYWORDS : Spectra, FLAT creation
!.OUTPUT   :
!.PURPOSE  :
!      To create flat
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_FLAT_norm_cre input output
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/Spec_ext.prg,v 1.1 2009/07/04 1
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
    define/par P1 ?             I   "Name for the input spectrum: "
    define/par P2 ?             I   "Name for the output spectrum: "
    define/par P3 1,1,0.0       C   "Scale: "
	!
	!...Lets define our variables
	!
    define/local  iname/c/1/60  "{p1}"
    define/local  oname/c/1/60  "{p2}"
    define/local  scale/c/1/60  "{p3}"
    define/local  i/i/1/1        0
	!
	!...main body of program
	!
    filter/median {iname} FLAT_b {scale}
    comp/ima FLAT_n = {iname}/FLAT_b
    stat/ima FLAT_n
    comp/ima FLAT_n = FLAT_n/{outputr(8)}
    stat/ima FLAT_n
    outd/fits FLAT_n.bdf {oname}
	!
   -delete FLAT_b.bdf FLAT_n.bdf
