!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2011 SAAO, Cape Town
!.IDENT    : RSS_Gain_cor.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Gain_cor
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Additional correction of GAIN for RSS spectra
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Gain_cor MIDAS_catalogue
! where:
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_Gain_cor.prg,v 1.5 2019/06/22 18:22:03 akniazev Exp akniazev $
!          : $Revision: 1.5 $
!-------------------------------------------------------------------------
!
    DEFINE/PAR P1 ? C "Enter_Catalogue_File_Name:"
!
    DEFINE/LOCAL J/I/1/2 0,0
    DEFINE/LOCAL CATAL/I/1/1  0
    DEFINE/LOCAL INPUT/C/1/80 "" ALL
    define/local oname/c/1/60 "IMAGE_TMP"
    DEFINE/LOCAL GAINTAB/C/1/80 "" ALL
    DEFINE/LOCAL REF/C/1/4 "    "
    DEFINE/LOCAL REF_SP/C/1/80 "" ALL
    DEFINE/LOCAL INCAT/C/1/20 "                    "
    DEFINE/LOCAL II/I/1/2 0,0
    DEFINE/LOCAL I/I/1/2 0,0
    DEFINE/LOCAL gainame/c/1/60 " "
    CLEAR/ICAT
	!
	!...test, if really catalog name was entered...
	!
    write/out "*********************************"
    write/out "******** GAIN correction ********"
    write/out "*********************************"
	    !
    SHOW/ICAT {P1}
    II(2) = {OUTPUTI(1)}
    II(1) = M$INDEX(P1,".cat")
    IF II(1) .LE. 0 THEN
       WRITE/KEY INCAT {P1}.cat
    ELSE
       WRITE/KEY INCAT {P1}
    ENDIF
	!
	!...Main loop
	!
    J(1) = 0
LOOP:
    STORE/FRAME INPUT {INCAT}
!    READ/KEY INPUT
!    WRITE/OUT input frame {INPUT}, input cat {INCAT}
!           ...
    J(1) = {J(1)} + 1
    write/out "***** {INPUT}: *****"
	!
	!...Will find the nearest gain-corrections table and copy it
	!   into the currect directory. Program will select the name
	!   taking into account gain, read-out and binning
	!
    outd/fits {INPUT} a.fits
    $LONG_RSS_gain.sh a.fits {dir_gain} | write/key gainame
    write/out "*****************************************************"
    if "{gainame}" .ne. "NONE" then
        cp -v {dir_gain}/{gainame} ./
    endif
    write/out "*****************************************************"
	!
	!...Make gain correction
	!
    write/out "***** {INPUT}: {{INPUT},GAINSET}+{{INPUT},ROSPEED} *****"
	    !
    if "{{INPUT},CCDSUM}" .EQ. "2 2" .OR. "{{INPUT},CCDSUM}" .EQ. "2 4" then
        write/out "***** {INPUT}: {{INPUT},CCDSUM} *****"
        copy/ii {INPUT} {oname}
        if M$EXIST("{gainame}") .eq. 1 then
            GAINTAB = "{gainame}"
            write/out "***** Gain correction for {oname} *****"
            comp/ima {oname}[<,<:@509,>]  = {oname}[<,<:@509,>]   / {{GAINTAB},:Corr,@1}
            comp/ima {oname}[<,<:@1050,>] = {oname}[<,<:@1050,>]  * {{GAINTAB},:Corr,@2}
            comp/ima {oname}[@1586,<:>,>] = {oname}[@1586,<:>,>]  * {{GAINTAB},:Corr,@3}
            comp/ima {oname}[@2145,<:>,>] = {oname}[@2145,<:>,>]  * {{GAINTAB},:Corr,@4}
            comp/ima {oname}[@2656,<:>,>] = {oname}[@2656,<:>,>]  * {{GAINTAB},:Corr,@5}
        endif
        copy/ii {oname} {INPUT}
    endif

    IF CATAL .LT. II(2) GOTO LOOP
END:
    -delete {oname}.bdf  a.fits
