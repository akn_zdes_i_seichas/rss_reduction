#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2020  SAAO, CapeTown
#.IDENT        RSS_epar.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Global definition of all parameteres for the RSS package
#
#.COMMENT
#.VERSION      $Date: 2020/06/22 15:35:32 $
#              $Revision: 1.15 $
############################################################################
#
#...Working directory
#
data_home="/home/DATA/PIs/TOWNSEND"
#
#...Name of LOG file
#
LOG=RSS_LOG.txt
#
#...Style of the work
#
Style=remote
#Style=local
#
#...Define method to clean cosmic: 
#   old - standard MIDAS method
#   new - method based on LAPLAC filter
#
cosmic=old
#
#...Define global variables for MIDAS
#
cat -> ${HOME}/midwork/RSS_Global.prg <<end1
write/key    dir_flat/c/1/80  "${data_home}/0ALL_FLATS"
write/key    dir_flatn/c/1/80 "${data_home}/0ALL_FLATnS"
write/key    dir_gain/c/1/80  "${data_home}/0ALL_GAINS"
write/key    cosm_meth/c/1/10 "${cosmic}"
end1
