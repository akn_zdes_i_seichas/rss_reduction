#!/bin/bash
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#.COPYRIGHT: (c) 2020 SAAO, Cape Town
#.IDENT    : RSS_reduction.prg
#.AUTHOR   : A.Y. Kniazev
#.CALL     : @@ RSS_reduction
#.CALL FROM:
#.KEYWORDS :
#.OUTPUT   :
#.PURPOSE  :
#      SALT semi-and-automatic data reduction.
#
#.COMMENTS :
#.USAGE    :
#      Execute as:
# @@ RSS_reduction [FILE=RSS_epar.sh] date=Date propid=program\
#                  [STEP=ALL/FLATS/EXTR/SPST/OBJ/FLUX] [EXIT=FLATS/...] [vis=Y/N]
# where:
#
#-------------------------------------------------------------------------
#
#
# ...Help
#
help() {
    echo ""
    echo "Usage: RSS_reduction.sh [FILE=RSS_epar.sh] date=Date propid=program" 1>&2 
    echo "       [STEP=ALL/FLATS/EXTR/SPST/OBJ/FLUX] [EXIT=FLATS/...] [vis=Y/N]" 1>&2
    echo "                                    " 1>&2
    echo "       Date      - is in yyyymmdd format" 1>&2
    echo "       Program   - is the name of science program in SALT PROPID term" 1>&2
    echo "       [ALL/...] - is the name of ENTRANCE point" 1>&2
    echo "       [NO/...]  - is the name of EXIT point (BEFORE specified point)" 1>&2
    echo "                 - Names are the same as for the ENTRANCE" 1>&2
    echo "       Y/N - without visualisation (N - default) or with (Y)" 1>&2
    echo ""
}
######################################################################
#
# ...Some initial setups
#
#...Global
#
STEP=ALL
EXIT=END
vis=N
DIR="/home/akniazev/midwork/RSS_reduction"
FILE=RSS_epar.sh
eval $*
if [ -f ${DIR}/${FILE} ]; then
    . ${DIR}/${FILE}
else
    echo " No configuration ${FILE} file in the directory ${DIR} *****"
    exit 1
fi
eval $*
######################################################################
#
# ...Check input parameters
#
#...For help
#
if [ "z${help}" = "zyes" -o $# -eq 0 ]; then
    help
    exit 1
fi
#
# ...For list of objects
#
if [ "z${date}" = "z" -o "z${propid}" = "z" ]; then
    echo ""
    echo "***** RSS_reduction.sh: no Date and/or PROPID were specified *****"
    echo ""
    help
    exit 1
fi
#
#...Main body
#
Date=${date}
Program=${propid}
year=`echo ${Date} | cut -b 1-4`
date=`echo ${Date} | cut -b 5-8`

dir=/salt/data/${year}/${date}/${Program}
#...data_home is pecified at RSS_epar.sh now
red_dir=${data_home}/${year}/${Date}/
reduction=${red_dir}/${Program}/product/Reduction/
    #
	#...(0) Start LOG
	#
#echo "" >${LOG}
#echo "***** Start Reduction for night = ${Date} *****" >>${LOG}
#echo "" >>${LOG}
	#
	#...(1) check that this data exist in the archive and copy them
	#   STEP=ALL
	#
if [ "$STEP" = "ALL" ] ; then
    if [ $Style = "local" ]; then
        if [ ! -d ${dir} ] ; then
            echo ""
            echo "Data for ${Program} for date ${Date} do not exist in SALT archive ..."
            echo ""
            exit 2
        fi
    fi
    if [ ! -d ${data_home}/${year} ] ; then
        mkdir ${data_home}/${year}
    fi
    if [ ! -d ${data_home}/${year}/${Date} ] ; then
        mkdir ${data_home}/${year}/${Date}
    fi
    echo "********** (1) Check RSS Data for ${Date} -> OK **********"
    echo "********** (1) Copying Data for ${Date} -> ... **********"
    cd ${red_dir}
    rm -rf ${Program}
    if [ $Style = "local" ]; then
        (cd /salt/data/${year}/${date};tar chf - ${Program})|tar xvf -
        if [ $? != 0 ] ; then exit 1; fi
    else
#         scp -r saltastro:/salt/data/${year}/${date}/${Program} ./
        scp -r sa@saltpipe1804:/salt/data/${year}/${date}/${Program} ./
#        if [ $? != 0 ] ; then exit 1; fi
    fi
	STEP=FLATS
fi
cd ${red_dir}/${Program}
if [ -d raw ]; then rm -rf ./raw; fi
cd product
rm -f P${Date}*.fits
if [ ! -d Reduction ]; then mkdir Reduction; fi
if [ "$STEP" = "ALL" -o "$STEP" = "FLATS" -o "$STEP" = "EXTR" ] ; then
   fz=$(ls -d mbxgpP*.fits.fz 2>/dev/null)
   if [ "z${fz}" != "z" ]; then
       funpack -D -v mbxgpP*.fits.fz
   fi
   cp -v mbxgpP*.fits Reduction
   fpack -D -Y -v m*.fits
   rm -f Reduction/*.fz
fi
if [ "${EXIT}" = "FLATS" ]; then
    find . -name m\*.fits -exec fpack -D -Y -v {} \;
    exit 1
fi
	#
	#...(2) Start preliminary reduction like GAINs+spectral FLATs preparation
	#

cd Reduction
if [ "$STEP" != "FLUX" ] ; then
    fz=$(ls -d mbxgpP*.fits.fz 2>/dev/null)
    if [ "z${fz}" != "z" ]; then
        funpack -D -v ${fz}
    fi
    fz=$(ls -d cosm*.fits.gz 2>/dev/null)
    if [ "z${fz}" != "z" ]; then
        gunzip -v ${fz}
    fi
fi
	    #
	    #...In case FLATs exist use them to calculate gain corrections and spectral flats
        #   I use mbxgpP${Date}*.fits because after EXTR only '*ee.fits' will be there
	    #
DESC="BVISITID"
#DESC="BLOCKID"
blocks=$(dfits mbxgpP${Date}*.fits|fitsort ${DESC}|tail -n +2|awk '{print $2}'|sort -u)
if [ "z${blocks}" = "z" ]; then
    echo "***** Old FITS files with no BVISITID descriptor. Use BLOCKID instead..."
    DESC="BLOCKID"
fi
flats=`dfits mbxgpP${Date}*.fits|fitsort OBJECT|awk '/FLAT/{print $1}'`
if [ "$STEP" = "FLATS" ] ; then
    if [ "z${flats}" != "z" ] ; then
	blocks=$(dfits mbxgpP${Date}*.fits|fitsort ${DESC}|tail -n +2|awk '{print $2}'|sort -u)
	for i in ${blocks}
	do
        block=$(dfits mbxgpP*.fits|fitsort object ${DESC}|tail -n +2|fgrep ${i}|fgrep FLAT|awk '{print $1}')
	    if [ "z${block}" != "z" ] ; then
            if [ -d FLAT_${i} ]; then rm -rf FLAT_${i}; fi
            mkdir FLAT_${i}
            mv -v ${block} FLAT_${i}
            cd FLAT_${i}
            #
            #...We need to split the total list of FLATs
            #   into different configurations 
            #
            rm -f List.txt
            cp -v ~/bin/LONG_epar.sh ./LONG_epar.sh
            for j in `echo ${block}`
            do
                LONG_Make_setup.sh ${j} >>List.txt
                echo " ${j}" >>List.txt
            done
            rm -f obj?.lst obj?_tmp.lst 
            awk -F \_ '{print $1,$2,$3,$4,$5,$6}' List.txt|\
            awk '{print $7,$2,$3,$5,$1}'|sort -k 2,4|\
            awk 'BEGIN{set=1; obj="obj";}
            {if (NR==1){
                    gr=$2; aa=$3; bin=$4;
                    obj_out=(obj set);
                    print $0 >>obj_out"_tmp.lst";
                    print $1 >>obj_out".lst";
                }
                else{if (gr==$2 && aa==$3 && bin==$4) {
                        print $0 >>obj_out"_tmp.lst";
                        print $1 >>obj_out".lst";
                    }
                    else {set++; gr=$2; aa=$3; bin=$4;
                        obj_out=(obj set);
                        print $0 >>obj_out"_tmp.lst";
                        print $1 >>obj_out".lst";
                    }
                }
            }'                                  
            rm -f List.txt
            for j in obj?.lst
            do
                dir_name=$(echo ${j}|sed 's/.lst//g'|tr [a-z] [A-Z])
                if [ -d ${dir_name} ]; then rm -rf ${dir_name}; fi
                mkdir ${dir_name}
                cp $(cat ${j}) ${dir_name}
                cd ${dir_name}
                inmidas 55 -p -j "@@ RSS_Extract mbxgpP${Date} [<,<:>,>] GAIN; wait 2; bye"
                if [ $? != 0 ] ; then exit 1; fi
                inmidas 55 -p -j "@@ RSS_Extract mbxgpP${Date} [<,<:>,>] FLAT; wait 2; bye"
                if [ $? != 0 ] ; then exit 1; fi
                mv * ../
                cd ../
                rm -rf ${dir_name}
            done
            rm -f FORGRdrs.KEY middummclear.prg mbxgpP20[12]?????????.fits *.cat
            rm -f mbxgpP*ee.fits uparmimhimcome.par LONG_epar.sh Gain_cor.tbl
            cd ../
	    fi
	done
    fi
    STEP=EXTR
else
    rm -f ${flats}
fi
if [ "${EXIT}" = "EXTR" ]; then 
    find . -name m\*.fits -exec fpack -D -Y -v {} \;
    find . -name cosm\*.fits -exec gzip -v {} \;
    exit
fi
	    #
	    #...Continue with objects, standards and arcs
	    #   Setup working area based on the binning factor
	    #   Iteratively we reduce all possible binnings here
	    #
if [ "$STEP" = "EXTR" ] ; then
    for i in 2x4 2x2
    do
        List=$(dfits mbxgpP${Date}????.fits|fitsort CCDSUM|tail -n +2|awk '{printf "%s %1sx%1s\n",$1,$2,$3}'|awk "/${i}/{print \$1}")
        if [ "z${List}" != "z" ]; then
            if [ "${i}" = "2x2" ] ; then
                BIN="[<,100:>,1950]"
            elif [ "${i}" = "2x4" ] ; then
                BIN="[<,@50:>,@950]"
#                 BIN="[<,@100:>,@900]"
            else
                echo "***** Unknown BINNING ..."
                exit 1
            fi
            mkdir $i
            mv -v `echo ${List}` $i
            cd $i
            inmidas 55 -p -j "@@ RSS_Extract mbxgpP${Date} ${BIN} OBJECT; wait 2; bye"
            if [ $? != 0 ] ; then exit 1; fi
            rm -f FORGRdrs.KEY middummclear.prg mbxgpP20[12]?????????.fits *.cat
            mv * ../
            cd ../
            rm -rf $i
            rm -f *.bdf dirfile.ascii
        fi
    done
    STEP=SPST
fi
if [ "${EXIT}" = "SPST" ]; then
    find . -name m\*.fits -exec fpack -D -Y -v {} \;
    find . -name cosm\*.fits -exec gzip -v {} \;
    exit
fi
	    #
	    #...(3) Start Long-slit reduction
	    #
	    #...1. Spectrophotometric standard in case it was observed in the same night
	    #
if [ "${STEP}" = "SPST" ] ; then
    spst=`dfits m*ee.fits|fitsort propid|awk '/CAL_SPST/{print $1}'`
    if [ "z${spst}" != "z" ] ; then
	cp -v ~/bin/LONG_epar.sh.OBJECT ./LONG_epar.sh
	for i in ${spst}
	do
	    echo $i >std.lst
	    LONG_red.sh std=std.lst
        if [ $? != 0 ] ; then exit 1; fi
        done
    fi
    rm -f Std_*
    STEP=OBJ
fi
if [ "${EXIT}" = "OBJ" ]; then 
    find . -name m\*.fits -exec fpack -D -Y -v {} \;
    find . -name cosm\*.fits -exec gzip -v {} \;
    exit
fi
	    #
	    #...2. Objects
	    #   Currently directory can contain many different blocks related to the same proposal,
	    #   Program can not work yet in case when ONE block consist of different setups
	    #
if [ "${STEP}" = "OBJ" ] ; then
		#
		#...Just check that we do not have all data in the compression form here
		#
    fz=$(ls -d mbxgpP${Date}????ee.fits.fz 2>/dev/null)
    if [ "z${fz}" != "z" ]; then
        funpack -D -v ${fz}
    fi
    fz=$(ls -d cosm*.fits.gz 2>/dev/null)
    if [ "z${fz}" != "z" ]; then
        gunzip -v ${fz}
    fi
		#
		#...And now we will start
		#
    blocks=$(dfits mbxgpP${Date}????ee.fits|fitsort ${DESC} propid object|\
        tail -n +2|egrep -v 'FLAT|ARC|CAL_SPST'|awk '{print $2}'|sort -u)
    if [ "z${blocks}" != "z" ]; then
        for j in ${blocks}
        do
            block=$(dfits mbxgpP${Date}????ee.fits|fitsort ${DESC} propid object|\
                tail -n +2|fgrep ${j}|egrep -v 'FLAT|CAL_SPST'|awk '{print $1}')
            if [ "z${block}" != "z" ] ; then
                if [ -d Block_${j} ]; then rm -rf Block_${j}; fi
                mkdir Block_${j}
                cp -v ${block} Block_${j}
                mask=$(echo ${block}|sed 's/mbxgp/cosm_mbxgp/g')
                cp ${mask} Block_${j} 2>/dev/null
                cd Block_${j}
                    #
                    #...We moved all data belong to this block and can start long-slit
                    #
                object=$(dfits mbxgpP${Date}????ee.fits|fitsort object OBSTYPE CCDTYPE|\
                    tail -n +2|fgrep -v CAL_SPST|awk '/OBJECT/{print $1}')
                arc_list=$(dfits mbxgpP${Date}????ee.fits|fitsort object OBSTYPE CCDTYPE|\
                    fgrep -v CAL_SPST|awk '/ARC/{print $1}')
                if [ "z${object}" != "z" ]; then
                    cp -v ~/bin/LONG_epar.sh.OBJECT ./LONG_epar.sh
                        #
                        #...We need to split the total list of spectra objects
                        #   and following arcs into different configurations 
                        #
                    rm -f List.txt
                    for i in `echo ${object} ${arc_list}`
                    do
                        LONG_Make_setup.sh ${i} >>List.txt
                        echo " ${i}" >>List.txt
                    done
                    rm -f obj?.lst arc?.lst obj?_tmp.lst arc?_tmp.lst
                    awk -F \_ '{print $1,$2,$3,$4,$5,$6}' List.txt|\
                    awk '{print $7,$2,$3,$5,$1}'|sort -k 2,4|\
                    awk 'BEGIN{set=1; obj="obj"; arc="arc";}
                    {if (NR==1){
                            gr=$2; aa=$3; bin=$4;
                            obj_out=(obj set);
                            arc_out=(arc set);
                            if ($5 == "NONE"){
                                print $0 >>obj_out"_tmp.lst";
                                print $1 >>obj_out".lst";
                            }
                            else {print $0 >>arc_out"_tmp.lst";
                                  print $1 >>arc_out".lst";
                                  print $1 >>obj_out".lst";
                              }
                        }
                        else     {if (gr==$2 && aa==$3 && bin==$4) {
                                if ($5 == "NONE"){
                                    print $0 >>obj_out"_tmp.lst";
                                    print $1 >>obj_out".lst";
                                }
                                else {print $0 >>arc_out"_tmp.lst";
                                    print $1 >>arc_out".lst";
                                    print $1 >>obj_out".lst";
                                }
                            }
                            else {set++; gr=$2; aa=$3; bin=$4;
                                obj_out=(obj set);
                                arc_out=(arc set);
                                if ($5 == "NONE"){
                                    print $0 >>obj_out"_tmp.lst";
                                    print $1 >>obj_out".lst";
                                }
                                else {print $0 >>arc_out"_tmp.lst";
                                    print $1 >>arc_out".lst";
                                    print $1 >>obj_out".lst";
                                }
                            }
                        }
                    }'                                  
                    rm -f List.txt      
                        #
                        #...Main procedure for all configurations in the same block
                        #   I suggest that we have <10 of different configurations
                        #   and only those configurations needs to be reduced
                        #   that have objects
                        #
                    for i in obj?.lst
                    do
                        obj=${i}
                        arc=$(echo ${obj}|sed 's/obj/arc/g')
                            #
                            #...Lists of mask frames need to be created
                            #   to produce wavelength calibrations for mask-frames.
                            #   BUT we need to remember about different configurations!!!
                            #
                        rm -f mask.lst maskw.lst
                        for l in $(cat ${obj})
                        do
                            if [ -f cosm_${l} ]; then
                                ls cosm_${l} >>mask.lst
                            fi
                        done
                        if [ -f mask.lst ]; then
                            sed 's/ee.fits/eew.fits/g' < mask.lst >maskw.lst
                            rm -f $(cat maskw.lst)
                        fi
                        if [ -f ${arc} ]; then
                            LONG_red.sh obj=${obj} arc=${arc}
                            if [ $? != 0 ] ; then exit 1; fi
                        else
                            LONG_red.sh obj=${obj}
                            if [ $? != 0 ] ; then exit 1; fi
                        fi
                            #
                            #...In case mask images exist, we can produce 
                            #   the final correction of the cosmic to them
                            #
                        cosm=$(ls -d cosm_*.fits 2>/dev/null)
                        if [ "z${cosm}" != "z" ] ; then
                            sed 's/ee.fits/eews.fits/g' < ${obj} >${obj}s
                            sed 's/ee.fits/eewsf.fits/g' < ${obj} >${obj}f
                            for k in `cat ${obj}s`
                            do
                                oi=`echo ${k}|sed 's/.fits/c.fits/g'`
                                mask=`echo cosm_${k}|sed 's/eews.fits/eew.fits/g'`
                                if [ -f ${k} -a -f ${mask} ]; then
                                    inmidas 55 -p -j "@@ RSS_coscor ${k} ${mask} ${oi}; wait 2; bye"
                                    if [ $? != 0 ] ; then exit 1; fi
                                    mv ${oi} ${k}
                                fi
                            done
                            for k in `cat ${obj}f`
                            do
                                oi=`echo ${k}|sed 's/.fits/c.fits/g'`
                                mask=`echo cosm_${k}|sed 's/eewsf.fits/eew.fits/g'`
                                if [ -f ${k} -a -f ${mask} ]; then
                                    inmidas 55 -p -j "@@ RSS_coscor ${k} ${mask} ${oi}; wait 2; bye"
                                    if [ $? != 0 ] ; then exit 1; fi
                                    mv ${oi} ${k}
                                fi
                            done
                        fi
                    done
                fi
                if [ "z${arc_list}" != "z" ] ; then
                    echo ${arc_list}| sed 's/ee.fits/eewsf.fits/g' >arcf.lst
                    rm -f `cat arcf.lst`
                fi
                rm -f *.cat middummclear.prg mask.lst maskw.lst *.lstf *.lsts
                rm -f obj?.lst arc?.lst obj?_tmp.lst arc?_tmp.lst
                cd ../
            fi
        done
    fi
fi
	    #
	    #...3. Specifically Flux calibration
	    #
if [ "${STEP}" = "FLUX" ] ; then
    if [ -f ./LONG_epar.sh ]; then
        . ./LONG_epar.sh
    else
        cp -v ~/bin/LONG_epar.sh.OBJECT ./LONG_epar.sh
        . ./LONG_epar.sh
    fi
    for j in `echo Block_*`
    do
        cd $j
        fz=$(ls -d mbxgpP${Date}*.fits.fz 2>/dev/null)
        if [ "z${fz}" != "z" ]; then
            funpack -D -v ${fz}
        fi        
        fz=$(ls -d Std*.fits.fz 2>/dev/null)
        if [ "z${fz}" != "z" ]; then
            funpack -D -v ${fz}
        fi
        object=$(dfits mbxgpP${Date}*ws.fits|fitsort object OBSTYPE PROPID|tail -n +2|fgrep -v CAL_SPST|awk '/OBJECT/{print $1}')
        for i in ${object}
        do
                #
                # ...Just one object
                #
            echo ${i} >obj.lst
            setup=`LONG_Make_setup.sh ${i} std`
            closest=`LONG_Find_nearest.sh ${setup} std`
            if [ "z${closest}" != "zNONE" ]; then
                cp ${St_SPST}/${System}/${closest} ./
                LONG_flux.sh obj=obj.lst sens_file=${closest}
                if [ $? != 0 ] ; then exit 1; fi
            fi
        done
        rm -f obj.lst
        fz=$(ls -d mbxgpP${Date}*.fits 2>/dev/null)
        if [ "z${fz}" != "z" ]; then
            fpack -D -Y -v ${fz}
        fi
        cd ../
    done
fi
	#
	#...(3) Ideally, should be here the copy everything into archive?
	#
    #
    #...The end
    #
find . -name cosm\*.fits -exec gzip -v {} \;
find . -name \*.fits -exec fpack -D -Y -v {} \;
rm -rf pyraf/ List.txt
