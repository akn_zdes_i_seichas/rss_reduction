!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2018 SAAO, Cape Town
!.IDENT    : RSS_Gain_cor_calc.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Gain_cor_cal
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Clculation of additional correction of GAIN for RSS spectra
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Gain_cor_cal FLAT.fits
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_Gain_cor_calc.prg,v 1.4 2020/02/07 10:51:36 akniazev Exp akniazev $
!          : $Revision: 1.4 $
!-------------------------------------------------------------------------
!
    define/par p1 FLAT.fits  C "Input FITS-file name:"
    define/par p2 @100,@800  C "Row range for the average:"
    define/par p3 Gain_cor   C "Output table name:"
    define/par p4 N          C "Visualize[Y/y] or not [N/n]:"
	!
	!...Define our local variables
	!
    define/local iname/c/1/20  "{p1}"
    define/local range/c/1/20  "{p2}"
    define/local oname/c/1/20  "{p3}"    
    define/local vis/c/1/1     "{p4}"
    define/local yes/c/1/1     "Y"
    define/local r/r/1/2       0.,0.
    define/local rr/r/1/5      0. all
    define/local ratio/r/1/6   0.,0.,0.,0.,0.,0.
    define/local rat_err/r/1/6  0.,0.,0.,0.,0.,0.
	!
	!...Main
	!
    set/gra color=1
    indis/fits {iname} FLAT.bdf
	    !
	    !...Try to SORT/COL
	    !
    sort/col FLAT FLATs
!    repla/ima FLATs FLATsr <,0=0.
    aver/row Line = FLATs {range}
	    !
    crea/tab {oname} 3 5
    crea/col {oname} :Corr  F6.4
    crea/col {oname} :Cerr  F6.4
    crea/col {oname} :Lev   F7.1
	    !
	    !...Between A1 and A2
	    !
    extra/ima tmp1 = Line [@465:@500]
    extra/ima tmp2 = Line [@515:@550]
    copy/it  tmp1 tmp1 :WAVE
    name/col tmp1 #2 :int
    copy/it  tmp2 tmp2 :WAVE
    name/col tmp2 #2 :int
	    !
	    !...Compute regressions
	    !
    regression/polynomial tmp1 :int :wave 1
    save/regression       tmp1 coef
    compute/regression    tmp1 :fit = coef
	    !
    regression/polynomial tmp2 :int :wave 1
    save/regression       tmp2 coef
    compute/regression    tmp2 :fit = coef
	    !
    r(1) = {tmp1.tbl,coefd(1)} + {tmp1.tbl,coefd(2)}*509.
    r(2) = {tmp2.tbl,coefd(1)} + {tmp2.tbl,coefd(2)}*509.
    ratio(1)   =   {r(1)}/{r(2)}
    rat_err(1) = {ratio(1)} * m$sqrt(({tmp1.tbl,coefr(5)}/{r(1)})**2 + ({tmp2.tbl,coefr(5)}/{r(2)})**2)
    rr(1) = {r(1)}
    comp/tab tmp1 :fit1 = :fit/{ratio(1)}
    write/out "{ratio(1)} +/- {rat_err(1)}"
    comp/ima Line1 = Line
    comp/ima Line1[<:@509] = Line[<:@509] / {ratio(1)}

    if "{vis}" .eq. "Y" then
        @@ plot_akn1b Line
        co2
        over/tab tmp1 :wave :fit
        co4
        over/tab tmp2 :wave :fit
        co1
        over/tab tmp1 :wave :fit1
        co3
        over/row Line1
        co1
        inq/key yes/c/1/1 "Press <Enter>:"
    endif
!
    comp/ima Line = Line1
	    !
	    !...Between A2 and A3
	    !
    extra/ima tmp2 = Line [@950:@1000]
    extra/ima tmp3 = Line [@1080:@1125]
    copy/it  tmp2 tmp2 :WAVE
    name/col tmp2 #2 :int
    copy/it  tmp3 tmp3 :WAVE
    name/col tmp3 #2 :int
	    !
	    !...Compute regressions
	    !
    regression/polynomial tmp2 :int :wave 1
    save/regression       tmp2 coef
    compute/regression    tmp2 :fit = coef
	    !
    regression/polynomial tmp3 :int :wave 1
    save/regression       tmp3 coef
    compute/regression    tmp3 :fit = coef
	    !
    r(1) = {tmp2.tbl,coefd(1)} + {tmp2.tbl,coefd(2)}*1050.
    r(2) = {tmp3.tbl,coefd(1)} + {tmp3.tbl,coefd(2)}*1050.
    ratio(2) = {r(2)}/{r(1)}
    rat_err(2) = {ratio(2)} * m$sqrt(({tmp2.tbl,coefr(5)}/{r(1)})**2 + ({tmp3.tbl,coefr(5)}/{r(2)})**2)
    rr(2) = {r(1)}
    comp/tab tmp2 :fit1 = :fit * {ratio(2)}
    write/out "{ratio(2)} +/- {rat_err(2)}"
    comp/ima Line1 = Line
    comp/ima Line1[<:@1050] = Line[<:@1050] * {ratio(2)}
!
    if "{vis}" .eq. "Y" then
        @@ plot_akn1b Line
        co2
        over/tab tmp2 :wave :fit
        co4
        over/tab tmp3 :wave :fit
        co1
        over/tab tmp2 :wave :fit1
        co3
        over/row Line1
        co1
        inq/key yes/c/1/1 "Press <Enter>:"
    endif
    comp/ima Line = Line1
	    !
	    !...Between A3 and A4
	    !
    extra/ima tmp3 = Line [@1545:@1580]
    extra/ima tmp4 = Line [@1595:@1635]
    copy/it  tmp3 tmp3 :WAVE
    name/col tmp3 #2 :int
    copy/it  tmp4 tmp4 :WAVE
    name/col tmp4 #2 :int
	    !
	    !...Compute regressions
	    !
    regression/polynomial tmp3 :int :wave 1
    save/regression       tmp3 coef
    compute/regression    tmp3 :fit = coef
	    !
    regression/polynomial tmp4 :int :wave 1
    save/regression       tmp4 coef
    compute/regression    tmp4 :fit = coef
	    !
    r(1) = {tmp3.tbl,coefd(1)} + {tmp3.tbl,coefd(2)}*1586.0
    r(2) = {tmp4.tbl,coefd(1)} + {tmp4.tbl,coefd(2)}*1586.0
    ratio(3) = {r(1)}/{r(2)}
    rat_err(3) = {ratio(3)} * m$sqrt(({tmp3.tbl,coefr(5)}/{r(1)})**2 + ({tmp4.tbl,coefr(5)}/{r(2)})**2)
    rr(3) = {r(1)}
    comp/tab tmp4 :fit1 = :fit*{ratio(3)}
    write/out "{ratio(3)} +/- {rat_err(3)}"
    comp/ima Line1 = Line
    comp/ima Line1[@1586:>] = Line[@1586:>] * {ratio(3)}
    
    if "{vis}" .eq. "Y" then
        @@ plot_akn Line
        co2
        over/tab tmp3 :wave :fit
        co4
        over/tab tmp4 :wave :fit
        co1
        over/tab tmp4 :wave :fit1
        co3
        over/row Line1
        co1
        inq/key yes/c/1/1 "Press <Enter>:"
    endif

    comp/ima Line = Line1
	    !
	    !...Between A4 and A5
	    !
    extra/ima tmp4 = Line [@2040:@2085]
    extra/ima tmp5 = Line [@2165:@2210]
    copy/it  tmp4 tmp4 :WAVE
    name/col tmp4 #2 :int
    copy/it  tmp5 tmp5 :WAVE
    name/col tmp5 #2 :int
	    !
	    !...Compute regressions
	    !
    regression/polynomial tmp4 :int :wave 1
    save/regression       tmp4 coef
    compute/regression    tmp4 :fit = coef
	    !
    regression/polynomial tmp5 :int :wave 1
    save/regression       tmp5 coef
    compute/regression    tmp5 :fit = coef
	    !
    r(1) = {tmp4.tbl,coefd(1)} + {tmp4.tbl,coefd(2)}*2145.0
    r(2) = {tmp5.tbl,coefd(1)} + {tmp5.tbl,coefd(2)}*2145.0
    ratio(4) = {r(1)}/{r(2)}
    rat_err(4) = {ratio(4)} * m$sqrt(({tmp4.tbl,coefr(5)}/{r(1)})**2 + ({tmp5.tbl,coefr(5)}/{r(2)})**2)
    rr(4) = {r(1)}
    comp/tab tmp5 :fit1 = :fit*{ratio(4)}
    write/out "{ratio(4)} +/- {rat_err(4)}"
    comp/ima Line1 = Line
    comp/ima Line1[@2145:>] = Line[@2145:>] * {ratio(4)}

    if "{vis}" .eq. "Y" then
        @@ plot_akn Line
        co2
        over/tab tmp4 :wave :fit
        co4
        over/tab tmp5 :wave :fit
        co1
        over/tab tmp5 :wave :fit1
        co3
        over/row Line1
        co1
        inq/key yes/c/1/1 "Press <Enter>:"
    endif

    comp/ima Line = Line1
	    !
	    !...Between A5 and A6
	    !
    extra/ima tmp5 = Line [@2600:@2640]
    extra/ima tmp6 = Line [@2660:@2710]
    copy/it  tmp5 tmp5 :WAVE
    name/col tmp5 #2 :int
    copy/it  tmp6 tmp6 :WAVE
    name/col tmp6 #2 :int
	    !
	    !...Compute regressions
	    !
    regression/polynomial tmp5 :int :wave 1
    save/regression       tmp5 coef
    compute/regression    tmp5 :fit = coef
	    !
    regression/polynomial tmp6 :int :wave 1
    save/regression       tmp6 coef
    compute/regression    tmp6 :fit = coef
	    !
    r(1) = {tmp5.tbl,coefd(1)} + {tmp5.tbl,coefd(2)}*2656.0
    r(2) = {tmp6.tbl,coefd(1)} + {tmp6.tbl,coefd(2)}*2656.0
    ratio(5) = {r(1)}/{r(2)}
    rat_err(5) = {ratio(5)} * m$sqrt(({tmp5.tbl,coefr(5)}/{r(1)})**2 + ({tmp6.tbl,coefr(5)}/{r(2)})**2)
    rr(5) = {r(1)}
    comp/tab tmp6 :fit1 = :fit*{ratio(5)}
    write/out "{ratio(5)} +/- {rat_err(5)}"
    comp/ima Line1 = Line
    comp/ima Line1[@2656:>] = Line[@2656:>] * {ratio(5)}

    if "{vis}" .eq. "Y" then
        @@ plot_akn Line
        co2
        over/tab tmp5 :wave :fit
        co4
        over/tab tmp6 :wave :fit
        co1
        over/tab tmp6 :wave :fit1
        co3
        over/row Line1
        co1
        inq/key yes/c/1/1 "Press <Enter>:"
    endif

    comp/ima Line = Line1
!
!@@ plot_akn1b Line
!exit
	!
	!...the End
	!
	    !...Coefficients
	    !
    {oname}.tbl,:Corr,@1 = {ratio(1)}
    {oname}.tbl,:Corr,@2 = {ratio(2)}
    {oname}.tbl,:Corr,@3 = {ratio(3)}
    {oname}.tbl,:Corr,@4 = {ratio(4)}
    {oname}.tbl,:Corr,@5 = {ratio(5)}
	    !
	    !...Errors
	    !
    {oname}.tbl,:Cerr,@1 = {rat_err(1)}
    {oname}.tbl,:Cerr,@2 = {rat_err(2)}
    {oname}.tbl,:Cerr,@3 = {rat_err(3)}
    {oname}.tbl,:Cerr,@4 = {rat_err(4)}
    {oname}.tbl,:Cerr,@5 = {rat_err(5)}
	    !
	    !...Level
	    !
    {oname}.tbl,:Lev,@1 = {rr(1)}
    {oname}.tbl,:Lev,@2 = {rr(2)}
    {oname}.tbl,:Lev,@3 = {rr(3)}
    {oname}.tbl,:Lev,@4 = {rr(4)}
    {oname}.tbl,:Lev,@5 = {rr(5)}
    read/tab {oname} :Corr,:Cerr,:Lev
    -delete tmp?.bdf tmp?.tbl FLAT.bdf FLATs.bdf FLATsr.bdf Line1.bdf Line.bdf
