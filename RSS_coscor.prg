!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2020 SAAO, Cape Town
!.IDENT    : RSS_coscor
!.AUTHOR   : A.Y. Kniazev
!.CALL     : RSS_coscor
!.CALL FROM:
!.KEYWORDS : RSS reduction
!          :
!.OUTPUT   :
!.PURPOSE  :
!      Standard RSS reduction: correction for the cosmic finally
!
!.USAGE    :
!      Execute as:
!       RSS_coscor input_image mask output_image
!
!-------------------------------------------------------------------------
!
    define/par p1 ?     C "Input 2D spectrum:"
    define/par p2 ?     C "Input 2D mask:"
    define/par p3 ?     C "Output 2D spectrum:"
    define/par p4 6,6   N "Filter sizes [X,Y]:"
    define/par p5 N     C "Visualize or not each step:"
    define/par p6 2     N "Amount of seconds to wait in case of visualisation"
	!
    define/local iname/c/1/60  "{p1}"
    define/local itmp/c/1/60   ""
    define/local mask/c/1/60   "{p2}"
    define/local mtmp/c/1/60   ""
    define/local oname/c/1/60  "{p3}"
    define/local otmp/c/1/60   ""
    define/local size/r/1/2     {p4}
    define/local viz/c/1/1     "{p5}"
    define/local sec/r/1/1      {p6}
    define/local scale/C/1/5   "-2,-2"
    define/local i/i/1/1        0
	!
	!...Main body
	!
	    !...Import FITS to bdf
	    !
    if m$index("{iname}",".fits") .ne. 0  then
        i = m$len("{iname}") - 5
        itmp = "{iname(1:{i(1)})}"
        indis/fits {iname} {itmp}
        iname = "{itmp}"
    endif
    if m$index("{mask}",".fits") .ne. 0  then
        i = m$len("{mask}") - 5
        mtmp = "{mask(1:{i(1)})}"
        indis/fits {mask} {mtmp}
        mask = "{mtmp}"
    endif
    if m$index("{oname}",".fits") .ne. 0  then
        i = m$len("{oname}") - 5
        otmp = "{oname(1:{i(1)})}"
    else
        otmp = "{oname}"
        oname = "{oname}.fits"
    endif
	    !
	    !...Use mask to replace pixel values
	    !
    if "{viz}" .ne. "N" then
        write/out "**** Imput image *****"
        load/ima {iname} 0 {scale} cuts=D,3sigma
    endif
    if "{{iname},CCDSUM}" .EQ. "2 2" .OR. "{{iname},CCDSUM}" .EQ. "2 4" then
        extra/ima CCD1  = {iname} [<,<:@1020,>]
        extra/ima CCD2  = {iname} [@1075,<:@2095,>]
        extra/ima CCD3  = {iname} [@2145,<:>,>]
        extra/ima MASK1 = {mask}  [<,<:@1020,>]
        extra/ima MASK2 = {mask}  [@1075,<:@2095,>]
        extra/ima MASK3 = {mask}  [@2145,<:>,>]
        @c lbl_new MASK1  MASK1l  N 0,0 0,0 0,0
        @c lbl_new MASK2  MASK2l  N 0,0 0,0 0,0
        @c lbl_new MASK3  MASK3l  N 0,0 0,0 0,0
        @c bkg_new CCD1 MASK1l CCD1e X 20 L
        @c bkg_new CCD2 MASK2l CCD2e X 20 L
        @c bkg_new CCD3 MASK3l CCD3e X 20 L
        copy/ii {iname}  {otmp}
        insert/ima CCD1e {otmp}
        insert/ima CCD2e {otmp}
        insert/ima CCD3e {otmp}
    else
        filter/median {iname} tmpf {size(1)},{size(2)},0.
        replace/ima {iname} {otmp} {mask}/0.1,>=tmpf
    endif
    if "{viz}" .ne. "N" then
        write/out "**** Result frame after cleaning *****"
        load/ima {otmp} 0 {scale} cuts=D,3sigma
    endif
    outd/fits {otmp} {oname}
	!
	!..End
	!
    -delete tmpf.bdf {itmp}.bdf {mtmp}.bdf {otmp}.bdf
    -delete CCD?.bdf CCD?e.bdf MASK?.bdf MASK?l.bdf
