#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2020  SAAO, CapeTown
#.IDENT        RSS_epar.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Global definition of all parameteres for the RSS package
#
#.COMMENT
#.VERSION      $Date: 2020/06/22 15:35:32 $
#              $Revision: 1.15 $
############################################################################
#
#...Working directory
#
data_home="/home/DATA/Data/SALT_data"
#data_home="/home/akniazev/Data/SALT_data"
red_dir="${data_home}/${year}/${Date}"
reduction="${red_dir}/${Program}/product/Reduction/"
#
#...Name of LOG file
#
LOG=RSS_LOG.txt
#
#...Style of the work
#
Style=remote
#Style=local
#
#...Define method to clean cosmic: 
#   old - standard MIDAS method
#   new - method based on LAPLAC filter
#
#cosmic=old
cosmic=new
#
#...Parameters for new cosmic method: 
#
par1=100    #- Sigma to cut intensities brighter than this limit BEFORE Laplace
par2=0     #- Sigma to select cosmic after Laplace filter 
par3=0     #- Sigma to select cosmic with use of SNR image 
par4=300   #- maximum length for cosmic in X direction (if 0 - ignore)
par5=300   #- maximum length for cosmic in Y direction (if 0 - ignore)
it1=1      #- amount of additional iterations to grow final mask for CCD1
it2=1      #- amount of additional iterations to grow final mask for CCD2
it3=1      #- amount of additional iterations to grow final mask for CCD3
smo1=999   #- use not median smooth to replace cosmic but background from AIP (if 999)
smo2=5     #- the size of median filter (2*smo2 + 1) in pixels
#
#...Define global variables for MIDAS
#
cat -> ${HOME}/midwork/RSS_Global.prg <<end1
write/key    dir_flat/c/1/80  "${data_home}/0ALL_FLATS"
write/key    dir_flatn/c/1/80 "${data_home}/0ALL_FLATnS"
write/key    dir_gain/c/1/80  "${data_home}/0ALL_GAINS"
write/key    cosm_meth/c/1/10 "${cosmic}"
write/key    par/r/1/5 ${par1},${par2},${par3},${par4},${par5}
write/key    it/i/1/3 ${it1},${it2},${it3}
write/key    smo/i/1/2 ${smo1},${smo2}
end1
