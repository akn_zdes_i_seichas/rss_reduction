##########################################################################
# .COPYRIGHT:   Copyright (c) 2020 South African Astronomical Observatory
#						all rights reserved
# .TYPE		make file
# .NAME         makefile
# .LANGUAGE	makefile syntax
# .ENVIRONMENT  Unix Systems
# .COMMENT      Instals files for the gain calculation
#
# .REMARKS	
# .AUTHOR       A.Y.Kniazev
##########################################################################
    #
    #...Setup
    #
LN = ln -sf
BINDIR  = ${HOME}/bin
PROCDIR = ${HOME}/midwork

all:
	@(PCK=`pwd | sed -e 's/\/proc$$//' -e 's/^.*\///'`; \
	for file in *.prg; \
		do  $(LN) ./$$PCK/$$file $(PROCDIR)/$$file; \
	done)
	@(PCK=`pwd | sed -e 's/\/proc$$//' -e 's/^.*\///'`; \
	for file in *.sh; \
		do  $(LN) $(PROCDIR)/$$PCK/$$file $(BINDIR)/$$file; \
	done)

clean:
	@(OBJP=`ls *.prg` ; cd $(PROCDIR) ; rm -f $$OBJP)
	@(OBJS=`ls *.sh` ; cd $(BINDIR) ; rm -f $$OBJS)
