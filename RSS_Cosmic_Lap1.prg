!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2020 SAAO, Cape Town
!.IDENT    : RSS_Cosmic_New
!.AUTHOR   : A.Y. Kniazev
!.CALL     : HRS_filter_spec
!.CALL FROM:
!.KEYWORDS : Filter, HRS
!          :
!.OUTPUT   :
!.PURPOSE  :
!      Standard HRS reduction: filtering data to remove cosmic-like events
!
!.USAGE    :
!      Execute as:
!
!.VERSION  : $Header: /home/akniazev/midwork/HRS/RCS/HRS_combine.prg,v 1.9 2019/09/22 14:35:22 akniazev Exp akniazev $
!          : $Revision: 1.9 $
!-------------------------------------------------------------------------
!
    define/par p1 ?       C "Input 2D spectrum:"
    define/par p2 ?       C "Output 2D spectrum:"
    define/par p3 4.0,4.0 N "Filter sizes [X,Y]:"
    define/par p4 15.     N "Sigma to select cosmic with SNR frame:"
    define/par p5 mask    C "Image name with mask of found cosmic:"
    define/par p6 N       C "Visualize or not each step:"
    define/par p7 1.,2.5  N "Gain and Read-Out Noise:"
	!
    define/local iname/c/1/60  "{p1}"
    define/local oname/c/1/60  "{p2}"
    define/local size/r/1/2     {p3}
    define/local sigma/r/1/2    {p4}
    define/local mask/c/1/10   "{p5}"
    define/local viz/c/1/1     "{p6}"
    define/local ccd/r/1/2      {p7}
    define/local gain/r/1/1     {ccd(1)}
    define/local ron/r/1/1      {ccd(2)}
    define/local limit/r/1/1     0.
	!
	!...Main body
	!
!    indi/fits {iname}.fits {iname}
    if "{viz}" .ne. "N" then
	write/out "**** Imput image *****"
	load/ima {iname} 0 1 cuts=D,3sigma
    endif
    filter/digital {iname} FILTER 0,-1,0,-1,4.8,-1,0,-1,0 !...LAPLACE
    if "{viz}" .ne. "N" then
	write/out "**** Result of LAPLACE filter *****"
	load/ima FILTER 0 1 cuts=D,3sigma
    endif
    comp/ima tmp = FILTER * 0.0
    repla/ima tmp + FILTER/<,0=1
    repla/ima FILTER + <,0=0
    if "{viz}" .ne. "N" then
	write/out "**** Negative pixels are removed *****"
	load/ima FILTER 0 1 cuts=D,3sigma
    endif
    filter/median FILTER noise 2,2,0.
    comp/ima snr = (FILTER * {gain})/sqrt({gain}*noise + {ron}**2)
    if "{viz}" .ne. "N" then
	write/out "**** Signal-To-Noise image *****"
	load/ima snr 0 1 cuts=D,3sigma
    endif
    filter/median snr snrf 2,2,0.
    comp/ima snr = snr - snrf
    stat/ima snr OPTION=W !>Null
    limit = {snr,STATISTIC(8)} + {snr,STATISTIC(14)} * {sigma}
    write/out "##### Limit is {limit} #####"
    repla/ima tmp + snr/{limit},>=1
    if "{viz}" .ne. "N" then
	write/out "**** Mask frame selected after LAPLACE filter *****"
	load/ima tmp 0 1 cuts=0,1
    endif
	!
	!...Additional median frames
	!
    filter/median {iname} med3 1,1,0.
    filter/median med3    med7 3,3,0.
    comp/ima f = (med3 - med7)/FILTER
    comp/ima mask = {iname} * 0.
    replace/ima mask + f/0.01,>=1
load/ima {mask} 0 1 cuts=0,1
exit
    comp/ima tmp = tmp + mask
    @c bim_new tmp   {mask} C 8 3
!    @c bim_new tmp    tmp1 C 8 3
!    @c bim_new tmp1 {mask} D 4 1
    if "{viz}" .ne. "N" then
	write/out "**** Mask frame after topological manipulations *****"
	load/ima {mask} 0 1 cuts=0,1
    endif
	!
	!...Reject pixels based on the constructed mask-frame
	!
    filter/median {iname} tmpf {size(1)},{size(2)},0.
    replace/ima {iname} {oname} {mask}/1,>=tmpf
    if "{viz}" .ne. "N" then
	write/out "**** Result frame after cleaning *****"
	load/ima {oname} 0 1 cuts=D,3sigma
    endif
!    outd/fits {oname} {oname}.fits
	!
	!..End
	!
    -delete tmp.bdf tmp1.bdf FILTER.bdf snr.bdf snrf.bdf tmpf.bdf
