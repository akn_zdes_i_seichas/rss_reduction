!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2011 SAAO, Cape Town
!.IDENT    : RSS_GAIN_calc.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_GAIN_calc
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Calculations for additional corrections of GAIN in 2D RSS spectra
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_GAIN_calc
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/ldss_ContCre.prg,v 1.1 2003/04/05 10:42:30 akniazev Exp akniazev $
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
define/local aver/i/1/2   600,1400
define/local coef34/r/1/1 0.
define/local coef45/r/1/1 0.
define/local coef56/r/1/1 0.
define/local coef12/r/1/1 0.
!
!...Amplifier 4 is the reference one
!
    !
    !...Main
    !
indis/fits FLAT.fits FLAT
copy/ii FLAT FLATc
	!
	!...Between Amp34
	!
aver/col col1 = FLAT @1579,@1580
aver/col col2 = FLAT @1582,@1583
comp/ima rat = col2/col1
stat/ima rat OPTION=G
coef34 = {OUTPUTR(8)}
	!
aver/row Line = FLAT @{aver(1)},@{aver(2)}
copy/ii  Line Line1
comp/ima Line1[1020:1581] = Line1[1020:1581] * {coef34}
comp/ima FLATc[1020,<:1581,>] = FLATc[1020,<:1581,>] * {coef34}
	!
	!...Between Amp45
	!
aver/col col1 = FLATc @2088,@2090
aver/col col2 = FLATc @2143,@2145
comp/ima rat = col1/col2
stat/ima rat OPTION=G
coef45 = {OUTPUTR(8)}
	!
aver/row Line = FLATc @{aver(1)},@{aver(2)}
copy/ii  Line Line1
comp/ima Line1[2101:2651]     = Line1[2101:2651] * {coef45}
comp/ima FLATc[2101,<:2651,>] = FLATc[2101,<:2651,>] * {coef45}
	!
	!...Between Amp56
	!
aver/col col1 = FLATc @2630,@2640
aver/col col2 = FLATc @2655,@2665
comp/ima rat = col1/col2
stat/ima rat OPTION=G
coef56 = {OUTPUTR(8)}
	!
aver/row Line = FLATc @{aver(1)},@{aver(2)}
copy/ii  Line Line1
comp/ima Line1[2652:>]     = Line1[2652:>] * {coef56}
comp/ima FLATc[2652,<:>,>] = FLATc[2652,<:>,>] * {coef56}
	!
	!...Between Amp12
	!
aver/col col1 = FLATc @502,@504
aver/col col2 = FLATc @507,@509
comp/ima rat = col2/col1
stat/ima rat OPTION=G
coef12 = {OUTPUTR(8)}
	!
aver/row Line = FLATc @{aver(1)},@{aver(2)}
copy/ii  Line Line1
comp/ima Line1[<:505]     = Line1[<:505] * {coef12}
comp/ima FLATc[<,<:505,>] = FLATc[<,<:505,>] * {coef12}
	!
	!...Final
	!
write/out "coef34 = {coef34}"
write/out "coef45 = {coef45}"
write/out "coef56 = {coef56}"
write/out "coef12 = {coef12}"
