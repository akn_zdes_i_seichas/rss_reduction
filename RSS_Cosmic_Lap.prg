!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2020 SAAO, Cape Town
!.IDENT    : RSS_Cosmic_New
!.AUTHOR   : A.Y. Kniazev
!.CALL     : HRS_filter_spec
!.CALL FROM:
!.KEYWORDS : Filter, HRS
!          :
!.OUTPUT   :
!.PURPOSE  :
!      Standard HRS reduction: filtering data to remove cosmic-like events
!
!.USAGE    :
!      Execute as:
!
!.VERSION  : $Header: /home/akniazev/midwork/HRS/RCS/HRS_combine.prg,v 1.9 2019/09/22 14:35:22 akniazev Exp akniazev $
!          : $Revision: 1.9 $
!-------------------------------------------------------------------------
!
    define/par p1 ?            C "Input 2D spectrum:"
    define/par p2 ?            C "Output 2D spectrum:"
    define/par p3 4.0,4.0      N "Filter sizes [X,Y]:"
    define/par p4 3.0,3.0,10.,20,20,0 N "Sigma to select cosmic with frame [Laplace, SNR, X-length]:"
    define/par p5 mask         C "Image name with mask of found cosmic:"
    define/par p6 1.,2.5       N "Gain and Read-Out Noise:"
    define/par p7 N            C "Visualize or not each step:"
    define/par p8 5            N "Amount of seconds to wait in case of visualisation:"
	!
    define/local iname/c/1/60  "{p1}"
    define/local oname/c/1/60  "{p2}"
    define/local size/r/1/2     {p3}
    define/local sigma/r/1/6    {p4}
!
!..P4 parameter:
!...sigma(1) - Sigma to cut intensities brighter than this limit BEFORE Laplace
!...sigma(2) - Sigma to select cosmic after Laplace filter 
!...sigma(3) - Sigma to select cosmic with use of SNR image 
!...sigma(4) - maximum length for cosmic in X direction (if 0 - ignore)
!...sigma(5) - maximum length for cosmic in Y direction (if 0 - ignore)
!...sigma(6) - amount of additional iterations to grow final mask
!...
!
    define/local mask/c/1/10   "{p5}"
    define/local ccd/r/1/2      {p6}
    define/local viz/c/1/1     "{p7}"
    define/local sec/r/1/1      {p8}
    define/local gain/r/1/1     {ccd(1)}
    define/local ron/r/1/1      {ccd(2)}
    define/local limit/r/1/1     0.
    define/local scale/C/1/5   "-1,-1"
    define/local xlen/i/1/1     {sigma(4)}
    define/local ylen/i/1/1     {sigma(5)}
    define/local iter/i/1/1     {sigma(6)}
	!
	!...Main body
	!
!    indi/fits {iname}.fits {iname}
    if "{viz}" .ne. "N" then
        write/out "**** Imput image *****"
        load/ima {iname} 0 {scale} cuts=D,3sigma
        wait {sec}
    endif
	!
	!...Step 1. Filter like LAPLACE and some additional selection of negative pixels
	!
    stat/ima {iname} OPTION=W >Null
    limit = {{iname},STATISTIC(8)} + {{iname},STATISTIC(14)} * ({sigma(1)}+0.)
    write/out "Limit to reject bright pixels from analysis is >{limit}"
    replace/ima {iname} {iname}r {limit},>={limit}
    filter/digital {iname}r FILTER 0,-1,0,-1,4.7,-1,0,-1,0 !...LAPLACE
    if "{viz}" .ne. "N" then
        write/out "**** Result of LAPLACE filter *****"
        load/ima FILTER 0 {scale} cuts=D,3sigma
        wait {sec}
    endif
    comp/ima tmp = FILTER * 0.0
    comp/ima tmp_snr = FILTER * 0.0
    stat/ima FILTER OPTION=W >Null
    limit = {FILTER,STATISTIC(8)} - {FILTER,STATISTIC(14)} * {sigma(2)}
    write/out "Limit to select negative pixels after LAPLACE is <{limit}"
    repla/ima tmp + FILTER/<,{limit}=1
    if {xlen} .ne. 0 then
        @c lbl_new tmp  tmp1   L 0,{xlen} 0,0 0,0
        mv tmp1.bdf tmp.bdf
    endif
    if {ylen} .ne. 0 then
        @c lbl_new tmp  tmp1   L 0,0 0,{ylen} 0,0
        mv tmp1.bdf tmp.bdf
    endif
    repla/ima FILTER + <,{limit}=0
    if "{viz}" .ne. "N" then
        write/out "**** Was selected after LAPLACE filter *****"
        load/ima tmp  0 {scale} cuts=D,3sigma
        wait {sec}
    endif
	!
	!...Step 2. Selection from SNR image
	!
!    filter/median FILTER noise 2,2,0.
    filter/median FILTER noise 2,0,0.
    comp/ima snr = (FILTER * {gain})/sqrt({gain}*noise + {ron}**2)
    if "{viz}" .ne. "N" then
        write/out "**** Signal-To-Noise image *****"
        load/ima snr 0 {scale} cuts=0,100
        wait {sec}
    endif
    filter/median snr snrf 2,2,0.
    comp/ima snr = snr - snrf
    stat/ima snr OPTION=W >Null
    limit = {snr,STATISTIC(8)} + {snr,STATISTIC(14)} * {sigma(3)}
    write/out "Limit to select extend structure is {limit}"
    if "{viz}" .ne. "N" then
        write/out "**** Signal-To-Noise image after extraction of extended structures *****"
        load/ima snr 0 {scale} cuts=0,50
        wait {sec}
    endif
    repla/ima tmp_snr + snr/{limit},>=1
    if "{viz}" .ne. "N" then
        write/out "**** Structures that were selected from SNR image *****"
        load/ima tmp_snr 0 {scale} cuts=0,1
        wait {sec}
    endif
    if {xlen} .ne. 0 then
        @c lbl_new tmp_snr tmp_snr1 L 0,{xlen} 0,0 0,0
        mv tmp_snr1.bdf tmp_snr.bdf
    endif
    if {ylen} .ne. 0 then
        @c lbl_new tmp_snr tmp_snr1 L 0,0 0,{ylen} 0,0
        mv tmp_snr1.bdf tmp_snr.bdf
    endif
    comp/ima tmp = tmp_snr + tmp
    if "{viz}" .ne. "N" then
        write/out "**** The final Mask frame selected after LAPLACE filter *****"
        load/ima tmp 0 {scale} cuts=0,1
        wait {sec}
    endif
	!
	!...Step 3. Some additional topological operations with mask frame
	!
    @c lbl_new tmp  tmp1   L 0,0 0,0 2,1000
    @c bim_new tmp1 tmp2   C 8 3
    if {xlen} .ne. 0 then
        @c lbl_new tmp2 {mask} L 0,{xlen} 0,0 0,0
        mv {mask}.bdf tmp2.bdf
    endif
    if {ylen} .ne. 0 then
        @c lbl_new tmp2 {mask} L 0,0 0,{ylen} 0,0
        mv {mask}.bdf tmp2.bdf
    endif
    mv tmp2.bdf {mask}.bdf
    if {iter} .ne. 0 then
        @c bim_new {mask} tmp D 4 1
        mv tmp.bdf {mask}.bdf
    endif
    if {xlen} .ne. 0 then
        @c lbl_new {mask} tmp1 L 0,{xlen} 0,0 0,0
        mv tmp1.bdf {mask}.bdf
    endif
    if {ylen} .ne. 0 then
        @c lbl_new {mask} tmp1 L 0,0 0,{ylen} 0,0
        mv tmp1.bdf {mask}.bdf
    endif
    if "{viz}" .ne. "N" then
        write/out "**** Mask frame after topological manipulations *****"
        load/ima {mask} 0 {scale} cuts=0,1
        wait {sec}
    endif
	!
	!...Step 4. Reject pixels based on the constructed mask-frame
	!
    if {size(1)} .ne. 999 then
        filter/median {iname} tmpf {size(1)},{size(2)},0.
        replace/ima {iname} {oname} {mask}/1,>=tmpf
    else
        copy/ii {mask} tmp I2
        mv tmp.bdf {mask}.bdf
        @c lbl_new {mask}  {mask}1  N 0,0 0,0 0,0
        @c bkg_new {iname} {mask}1 {oname} X 10 L
    endif
    copy/ii {mask} tmp I2
    mv tmp.bdf {mask}.bdf
    if "{viz}" .ne. "N" then
        write/out "**** Result frame after cleaning *****"
        load/ima {oname} 0 {scale} cuts=D,3sigma
    endif
!    outd/fits {oname} {oname}.fits
	!
	!..End
	!
    -delete tmp.bdf tmp1.bdf tmp2.bdf FILTER.bdf snr.bdf snrf.bdf tmpf.bdf noise.bdf
    -delete tmp_snr.bdf tmp_snr1.bdf {mask}1.bdf {iname}r.bdf
