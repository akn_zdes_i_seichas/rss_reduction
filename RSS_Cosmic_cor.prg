!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2011 SAAO, Cape Town
!.IDENT    : RSS_Cosmic_cor.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Cosmic_cor
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Correction for bad pixels in RSS spectra
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Bad_cor
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/ldss_ContCre.prg,v 1.1 2003/04/05 10:42:30 akniazev Exp akniazev $
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
    DEFINE/PAR P1 ? C "Enter_Catalogue_File_Name:"
!
    DEFINE/LOCAL J/I/1/2 0,0
    DEFINE/LOCAL CATAL/I/1/1  0
    DEFINE/LOCAL INPUT/C/1/80 "" ALL
    define/local oname/c/1/60 "IMAGE_TMP"
    DEFINE/LOCAL REF/C/1/4 "    "
    DEFINE/LOCAL REF_SP/C/1/80 "" ALL
    DEFINE/LOCAL INCAT/C/1/20 "                    "
    DEFINE/LOCAL II/I/1/2 0,0
    DEFINE/LOCAL I/I/1/2 0,0
    DEFINE/LOCAL scale/C/1/5  "-2,-2"
    ! parameters from general par/r/1/5    0.,0.,0.,0.,0.
    ! parameters from general it/i/1/3     1,0,1
    ! parameters from general smo/i/1/2    5,5
    CLEAR/ICAT
	!
	!...test, if really catalog name was entered...
	!
    write/out "*********************************"
    write/out "***** Remove cosmic rays *****"
    write/out "*********************************"
	    !
    SHOW/ICAT {P1}
    II(2) = {OUTPUTI(1)}
    II(1) = M$INDEX(P1,".cat")
    IF II(1) .LE. 0 THEN
       WRITE/KEY INCAT {P1}.cat
    ELSE
       WRITE/KEY INCAT {P1}
    ENDIF
	!
	!...Main loop
	!
    J(1) = 0
LOOP:
    STORE/FRAME INPUT {INCAT}
    J(1) = {J(1)} + 1
    write/out "***** {INPUT}: *****"

    if "{{INPUT},CCDTYPE}" .eq. "OBJECT" .and. {{INPUT},O_TIME(7)} .gt. 10. then
        write/out "***** {INPUT}: {{INPUT},GAINSET}+{{INPUT},ROSPEED} *****"
        !
        if m$index("{INPUT}",".bdf") .ne. 0  then
            i(1) = m$len("{INPUT}") - 4
            INPUT = "{INPUT(1:{i(1)})}"
        endif
        !
        if "{cosm_meth}" .eq. "new" then
            if "{{INPUT},CCDSUM}" .EQ. "2 2" .OR. "{{INPUT},CCDSUM}" .EQ. "2 4" then
                extra/ima CCD1 = {INPUT} [<,<:@1020,>]
                extra/ima CCD2 = {INPUT} [@1075,<:@2095,>]
                extra/ima CCD3 = {INPUT} [@2145,<:>,>]
                    !
                @@ RSS_Cosmic_corN CCD1 CCD1e  cosmask1 {it(1)}
                @@ RSS_Cosmic_corN CCD2 CCD2e  cosmask2 {it(2)}
                @@ RSS_Cosmic_corN CCD3 CCD3e  cosmask3 {it(3)}
                !
                copy/ii {INPUT} {INPUT}e
                insert/ima CCD1e {INPUT}e
                insert/ima CCD2e {INPUT}e
                insert/ima CCD3e {INPUT}e
                comp/ima cosmask = {INPUT} * 0.0
                insert/ima cosmask1 cosmask
                insert/ima cosmask2 cosmask
                insert/ima cosmask3 cosmask
                copy/ii cosmask tmp I2
                mv tmp.bdf cosmask.bdf
                outd/fits cosmask.bdf cosm_{INPUT}e.fits
            else
                @@ RSS_Cosmic_Lap {INPUT} {INPUT}e 4,4 0.9,7.,0,1 cosmask 1.,2.5 N ?
            endif
        else
            crea/icat Tmp {INPUT}.bdf
!           @@ remcosm Tmp.cat ? 1. 2. 1.4 1.4  !... For imaging
            @@ remcosm Tmp.cat ? 1. 3. 1.9 1.5
!            @@ remcosm Tmp.cat ? 1. 3. 2.5 1.8
!            @@ remcosm Tmp.cat ? 1. 3. 2.5 2.8
!            @@ remcosm Tmp.cat ? 1. 3. 2.5 3.2
!            @@ remcosm Tmp.cat ? 1. 1.0 2.0 2.8
!            @@ remcosm Tmp.cat ? 1. 3. 3.0 1.9
!            @@ remcosm Tmp.cat ? 1. 3. 3.2 3.2
!!            @@ remcosm Tmp.cat ? 1. 3. 2.0. 3.9
!            @@ remcosm Tmp.cat ? 1. 10. 10. 4.0
!            @@ remcosm Tmp.cat ? 1. 10. 10. 10.0
        endif
    else
        crea/icat Tmp {INPUT}
        @@ remcosm Tmp.cat ? 1. 100. 10. 10.
    endif

    IF CATAL .LT. II(2) GOTO LOOP
END:
    -delete {oname}.bdf Tmp.cat
    -delete CCD?.bdf CCD?e.bdf
    -delete cosmask1.bdf cosmask2.bdf cosmask3.bdf cosmask.bdf
