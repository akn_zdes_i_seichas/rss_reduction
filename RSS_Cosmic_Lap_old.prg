!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2020 SAAO, Cape Town
!.IDENT    : RSS_Cosmic_New
!.AUTHOR   : A.Y. Kniazev
!.CALL     : HRS_filter_spec
!.CALL FROM:
!.KEYWORDS : Filter, HRS
!          :
!.OUTPUT   :
!.PURPOSE  :
!      Standard HRS reduction: filtering data to remove cosmic-like events
!
!.USAGE    :
!      Execute as:
!
!.VERSION  : $Header: /home/akniazev/midwork/HRS/RCS/HRS_combine.prg,v 1.9 2019/09/22 14:35:22 akniazev Exp akniazev $
!          : $Revision: 1.9 $
!-------------------------------------------------------------------------
!
    define/par p1 ?       C "Input 2D spectrum:"
    define/par p2 ?       C "Output 2D spectrum:"
    define/par p3 2.0,2.0 N "Filter sizes [X,Y]:"
    define/par p4 mask    C "Image name with mask of found cosmic:"
	!
    define/local iname/c/1/60  "{p1}"
    define/local oname/c/1/60  "{p2}"
    define/local size/r/1/2     {p3}
    define/local mask/c/1/10   "{p4}"
    define/local dis/r/1/1       0.
	!
	!...Main body
	!
!    indi/fits {iname}.fits {iname}
del/des {iname} lhcuts
load/ima {iname} 0 1 cuts=D,2sigma
    filter/digital {iname} FILTER LAPLACE
load/ima FILTER 0 1 cuts=D,2sigma
    comp/ima tmp = FILTER * 0.0
    repla/ima tmp + FILTER/<,0=1
load/ima tmp 0 1 cuts=0,1
    @c bim_new tmp  {mask} D 8 2
load/ima {mask} 0 1 cuts=0,1
	!
	!...Reject pixels based on the mask-frame
	!
    filter/median {iname} tmpf {size(1)},{size(2)},0.
    replace/ima {iname} {oname} {mask}/1,>=tmpf
load/ima {oname} 0 1 cuts=D,2sigma
!    outd/fits {oname} {oname}.fits
	!
	!..End
	!
    -delete tmp.bdf tmpf.bdf FILTER.bdf
