#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2019  SAAO, CapeTown
#.IDENT        LONG_RSS_gain.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Make output for the closest in time table with gain corrections
#              for the input file $1. All search is done in the directory $2
#
#.COMMENT
#.VERSION      $Date: 2019/06/22 16:49:53 $
#              $Revision: 1.1 $
############################################################################
#
    #
    #...Input parameters
    #
input_file=$1
dir=$2
    #
    #...Start main
    #
cp ~/bin/LONG_epar.sh ./
file=`LONG_Make_setup.sh ${input_file} gain`
rm -f ./LONG_epar.sh
    #
gain=`echo ${file}|awk -F '_' '{print $3}'`
read=`echo ${file}|awk -F '_' '{print $4}'`
bin=`echo ${file}|awk -F '_' '{print $5}'`
jd=`echo ${file}|awk -F '_' '{print $6}'`
list=`ls ${dir}|fgrep ${gain}|fgrep ${read}|fgrep ${bin}`
ls ${dir}|fgrep ${gain}|fgrep ${read}|fgrep ${bin}|sort -n -r -k 25,37 >List.txt
    #
name=$(awk -F \_ "BEGIN{dmax=100000.; jd=$jd}
    {time=substr(\$6,1,13);
	 dif=sqrt((time-jd)**2);
	 if (dif < dmax){dmax=dif; name=\$0;}
	}
    END  {print name}" List.txt)
if [ "z${name}" != "z" ]; then
    echo -n ${name}
else
    echo -n "NONE"
fi
