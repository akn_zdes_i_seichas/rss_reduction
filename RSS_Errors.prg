!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2019 SAAO, Cape Town
!.IDENT    : RSS_Errors.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Errors
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Calculations for errors on the base of spectrum before sensitivity
!      correction. Created second row for these errors
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Errors Input_Spec_1df Input_Spec_1d Output
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_Errors.prg,v 1.3 2019/06/15 21:10:37 akniazev Exp akniazev $
!          : $Revision: 1.3 $
!-------------------------------------------------------------------------
!
   define/par p1 ?   C "Input_spectrum_f:"
   define/par p2 ?   C "Input_spectrum_s:"
   define/par p3 ?   C "Output_spectrum_f_with_errors:"
   define/par p4 1.  N "Additional parameter:"
   define/par p5 N   C "Plot ot not result:"
	!
	!...Define our local variables
	!
    define/local inamef/c/1/80  "{p1}"
    define/local inames/c/1/80  "{p2}"
    define/local oname/c/1/80   "{p3}"
    define/local coeff/r/1/1     {p4}
    define/local readout/r/1/1    0.
    define/local num/i/1/1        20
    define/local plot/c/1/1     "{P5}"
	!
	!...Main body
	!
    indi/fits {inamef}.fits {inamef}  >Null
    indi/fits {inames}.fits {inames}  >Null
    if "{{inames},INSTRUME}" .eq. "RSS" then
		!
		!...For RSS data READOUT noise is taken as 2.5
		!
!        $dfits {inames}.fits|fgrep APNUM1|awk -F\' '{print $2}'|awk '{print int($4-$3)}' | write/key num
!        write/out "***** num = {num}"
	readout = 1.
	write/desc {inamef} READOUT/r/1/1 1.0
!        readout = 2.5
!        write/desc {inamef} READOUT/r/1/1 2.5
    else
	num = 10
	readout = 1.
	write/desc {inamef} READOUT/r/1/1 1.0
    endif
	    !
	    !...Compute ratio between these spectra and calculate error
	    !
    comp/ima ratio = sqrt(({inamef}/{inames})**2)
    comp/ima Errorf = sqrt(sqrt({inames}**2) + 3*{num}*({readout})**2) * ratio * {coeff}
    comp/ima snr = {inamef}/Errorf
!    stat/ima snr [4500:5500] >Null
    stat/ima snr [<:>] >Null
    write/desc snr SNR_MEAN/r/1/1 {outputr(3)}
    write/desc snr SNR_RMS/r/1/1 {outputr(4)}
    if "{plot}" .eq. "Y" then
	@@ plot_akn2 {inamef} Errorf
!        plot/row snr
    endif
	    !
	    !...Create 2D frame and put spectrum_f and its error there
	    !
    crea/ima a 2,{{inamef},NPIX},2 {{inamef},START},1.,{{inamef},STEP},1.
    insert/ima {inamef} a <,@1
    insert/ima Errorf   a <,@2
	    !
	    !...Copy all descriptors and move temporary file into output name
	    !
    copy/dd {inamef} *,3   a ?
    copy/dd {inamef} IDENT a ?
    copy/dd {inamef} CUNIT a ?
    copy/ii a {oname}
    copy/dd snr      SNR_MEAN {oname} SNR_MEAN
    copy/dd snr      SNR_RMS  {oname} SNR_RMS
    copy/dd {inamef} READOUT  {oname} READOUT
	    !
	    !...Create/copy a couple of descriptors for 'fbs' program
	    !
    @@ HRS/HRS_heliocor {oname} >Null
    set/format ,f14.6
    write/desc {oname} ENVMJD/d/1/1 {{oname},JD}
	    !
	    !...Write FITS-file
	    !
    outd/fits {oname}.bdf {oname}.fits
    set/format
	!
	!...End
	!
    -delete ratio.bdf Errorf.bdf a.bdf *.cat snr.bdf Mid*Pipe
    -delete {oname}.bdf {inamef} {inames}
