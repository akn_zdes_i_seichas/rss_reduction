!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2016 SAAO
!.IDENT    : PFIS_fcam.prg
!.AUTHOR   : A.Yu. Kniazev
!.CALL     : RSS_fcam.prg
!.CALL FROM:
!.KEYWORDS : RSS observations
!.OUTPUT   :
!.PURPOSE  :
!      calculation of the focus value for RSS camera
!.USAGE    :
!      Execute as:
!
!.VERSION  : $Header: /data/midas/RAC/Obsccd/proc/RCS/occd_fcalc_pf500.prg,v 1.1 1996/01/14 18:26:40 midas Exp $
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
    define/par p1 GR900         C "Input_GRATING (GR900):"
    define/par p2 13.63         C "Input grating angle (13.63):"
    define/par p3 27.25         C "Input articulation angle (27.25):"
    define/par p4 Hoya_UV-34    C "Filter name (without spaces):"
    define/par p5 3650-6770     C "Spectral range (3650-6770):"
    define/par p6 1.5           C "Slit size (1.5):"
    define/par p7 2             N "Polynom power (integer):"
	!
	!...Define our local variables
	!
    define/local grat/c/1/10  "{p1}"
    define/local grata/c/1/10 "{p2}"
    define/local arta/c/1/10  "{p3}"
    define/local filt/c/1/14  "{p4}"
    define/local range/c/1/10 "{p5}"
    define/local slit/c/1/10  "{p6}"
    define/local power/i/1/1   {p7}
    define/local tnum/i/1/1     0
    define/local yes/c/1/3     "on"
    define/local i/i/1/4        0
    define/local ii/i/1/3       0
    define/local fstart/r/1/1   0
    define/local fstep/r/1/1    0.
    define/local fmin/r/1/1     0.
    define/local fwhm/r/1/1     0.
    define/local mfwhm/r/1/1    0.
    define/local lim/r/1/2   0.,0.
    define/local lcount/i/1/1   0
	!
	!...Main body
	!
    lim(1) = {lin0001.tbl,:focus,@1}
    i(4)   = {lin0001.tbl,TBLCONTR(4)}
    lim(2) = {lin0001.tbl,:focus,@{i(4)}}
	    !
	    !...Prepare our our tables
	    !
    if m$exist("list_lines_{grat}_{grata}_{arta}.tbl") .eq. 1 then
	cp list_lines_{grat}_{grata}_{arta}.tbl list_lines.tbl
    endif
    if m$exist("lin0001_{grat}_{grata}_{arta}.tbl") .eq. 1 then
	do i(2) = 1 {i(4)} 1
	    cp lin{i(2)}_{grat}_{grata}_{arta}.tbl lin{i(2)}.tbl
	enddo
    endif
    i(4) = {list_lines.tbl,TBLCONTR(4)}
	    !
	    !...Compute regressions
	    !
    do i(2) = 1 {i(4)} 1
power:
		!
		!...a.Plot initial data
		!
	name/col lin{i(2)} :focus  ? "microns"
	name/col lin{i(2)} :fwhm   ? "pixels"
	name/col lin{i(2)} :center ? "pixels"
	set/graph color=1 ltype=0 stype=5 pmode=0 font=1 tsize=0.75
	plot/tab lin{i(2)} :focus :fwhm
	set/graph color=1 ltype=1 stype=0
	over/tab lin{i(2)} :focus :fwhm
		!
		!...b.Compute regression
		!
	inq/key lim/r/1/2 "Input limits for fitting area [min,max]: "
	select/tab lin{i(2)} :focus.gt.{lim(1)}.and.:focus.lt.{lim(2)}
	regression/polynomial lin{i(2)} :fwhm :focus {power}
	save/regression lin{i(2)} coef
	select/tab lin{i(2)} all
	compute/regression lin{i(2)} :fit = coef
		!
		!...c.Find focus value for minimum
		!
	stat/tab lin{i(2)} :focus
	fstep = (outputr(2)-outputr(1))/500.
	create/ima ref 1,500 {outputr(1)},{fstep} nodata
	convert/table focus = lin{i(2)} :focus :fit ref SPLINE
	stat/image focus option=fn
	mfwhm = {outputr(1)}
	set/grap color=2 ltype=1 stype=0
	over/row focus
	fmin = {focus,start(1)} + {focus,step(1)}*({outputi(2)} - 1)
	set/format f6.3
	write/out "Focus value for minimum fwhm ({mfwhm}) = {fmin} microns"
	write/out
	set/gra color=4
	label/gra "{range} \AA : GR={grat}, g={grata}, a={arta}, Filter={filt}, Slit={slit}" 330,410,sc ? 1.0 0
	set/format f6.1
	label/gra "Focus value for minimum fwhm ({mfwhm} pix) = {fmin} microns" 330,380,sc ? 1.0 0
	set/format f6.0
	label/gra "Line with center at {list_lines.tbl,:center,@{i(2)}} pixel" 330,360,sc ? 1.0 0
	set/format f6.3
	set/gra color=1
	copy/gra postscript
	mv postscript.ps LFWHM{i(2)}_{grat}_{grata}_{arta}.ps
		!
		!...d.Do you want to repeat?
		!
	inq/key yes "Do you want repeat with new power [y/n]: "
	if yes(1:1) .eq. "y" then
	    inq/key power/i/1/1  "Input new power value [was = {power}]:"
	    goto power
	endif
	list_lines.tbl,:focus,{i(2)} = {fmin}
	list_lines.tbl,:fwhm,{i(2)}  = {mfwhm}
		!
		!...e.Plot line position
		!
	set/graph color=1 ltype=0 stype=18 pmode=0
	plot/tab lin{i(2)} :focus :center
	set/graph color=1 ltype=1 stype=0
	over/tab lin{i(2)} :focus :center
	set/gra color=4
	label/gra "{range} \AA : GR={grat}, g={grata}, a={arta}, Filter={filt}, Slit={slit}" 330,410,sc ? 1.0 0
	set/gra color=1
	copy/gra postscript
	mv postscript.ps LPos{i(2)}_{grat}_{grata}_{arta}.ps
    enddo
	    !
	    !...Plot the final results:
	    !
		!
		!...1.The best FWHMs
		!
    name/col list_lines :focus  ? "microns"
    name/col list_lines :center ? "pixels"
    name/col list_lines :fwhm   ? "pixels"
    set/graph color=1 ltype=0 stype=21
    plot/tab list_lines :center :fwhm
    set/gra color=4
    label/gra "{range} \AA : GR={grat}, g={grata}, a={arta}, Filter={filt}, Slit={slit}" 330,410,sc ? 1.0 0
    set/gra color=1
    copy/gra postscript
    mv postscript.ps BestFWHM_{grat}_{grata}_{arta}.ps
		!
		!...2.The positions of the best focus
		!
    set/graph color=1 ltype=0 stype=18
    plot/tab list_lines :center :focus
    set/graph color=1 ltype=1 stype=0
    over/tab list_lines :center :focus
    stat/tab list_lines :focus
!    over/line 2 1580,{outputr(1)} 1580,{outputr(2)}
    over/line 2 3166,{outputr(1)} 3166,{outputr(2)}
    set/gra color=4
    label/gra "{range} \AA : GR={grat}, g={grata}, a={arta}, Filter={filt}, Slit={slit}" 330,410,sc ? 1.0 0
    set/gra color=1
	!
    regression/polynomial list_lines :focus :center 1
    save/regression list_lines coef
    select/tab list_lines all
    compute/regression list_lines :fit = coef
    set/gra color=2 ltype=1 stype=0
    over/tab list_lines :center :fit
    set/gra color=1
	!
    copy/gra postscript
    mv postscript.ps BestFocus_{grat}_{grata}_{arta}.ps
		!
		!...3.FWHMs distributions for different focuses
		!
    crea/tab tmp 2 {i(4)}
    crea/col tmp :center "pixels"
    crea/col tmp :fwhm   "pixels"
    i(3) = {lin0001.tbl,TBLCONTR(4)}
    do i(1) = 1 {i(3)} 1
	do i(2) = 1 {i(4)} 1
	    tmp.tbl,:center,@{i(2)} = {lin{i(2)}.tbl,:center,@{i(1)}}
	    tmp.tbl,:fwhm,@{i(2)}  = {lin{i(2)}.tbl,:fwhm,@{i(1)}}
	enddo
	set/graph color=1 ltype=0 stype=18
	plot/tab tmp :center :fwhm
	set/graph color=1 ltype=1 stype=0
	over/tab tmp :center :fwhm
	set/gra color=4
	label/gra "{range} \AA : GR={grat}, g={grata}, a={arta}, Filter={filt}, Slit={slit}" 330,410,sc ? 1.0 0
	set/format f4.0
	label/gra "Focus value = {lin0001.tbl,:focus,@{i(1)}} microns" 330,380,sc ? 1.0 0
	set/gra color=1
	copy/gra postscript
	mv postscript.ps Focus{lin0001.tbl,:focus,@{i(1)}}_{grat}_{grata}_{arta}.ps
	set/format
    enddo
	    !
end:
	    !
	    !...Save our tables
	    !
    cp list_lines.tbl list_lines_{grat}_{grata}_{arta}.tbl
    do i(2) = 1 {i(4)} 1
	cp lin{i(2)}.tbl lin{i(2)}_{grat}_{grata}_{arta}.tbl
    enddo
	    !
	    !...Compile picures 8 per page
	    !
		!...Positions
		!
    $comp8_p.sh LPos*_*_*_*.*.ps
    $mv pic1.ps LPos_{grat}_{grata}_{arta}_pic1.ps
    $mv pic2.ps LPos_{grat}_{grata}_{arta}_pic2.ps
		!
		!...FWHMs
		!
    $comp8_p.sh LFWHM*_*_*_*.*.ps
    $mv pic1.ps LFWHM_{grat}_{grata}_{arta}_pic1.ps
    $mv pic2.ps LFWHM_{grat}_{grata}_{arta}_pic2.ps
		!
		!...Focus
		!
    $comp8_p.sh Focus???_*_*_*.*.ps Focus1???_*_*_*.*.ps
    $mv pic1.ps Focus_{grat}_{grata}_{arta}_pic1.ps
    $mv pic2.ps Focus_{grat}_{grata}_{arta}_pic2.ps
	    !
	    !...Return some initial values
	    !
    set/graph
    set/graph pmode=1 font=1
