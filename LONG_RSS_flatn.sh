#!/bin/bash
#
############################################################################
#.COPYRIGHT    (c)  2019  SAAO, CapeTown
#.IDENT        LONG_RSS_flatn.sh
#.LANGUAGE     SHyRAF  :-)
#.AUTHOR       AKN   (Kniazev A.Y.), SALT
#
#.KEYWORDS
#.PURPOSE      Make output for the closest in time fits-file with normalized
#              FLAT for the input file $1.
#              All search is done in the directory $2
#
#.COMMENT
#.VERSION      $Date: 2019/06/22 18:15:31 $
#              $Revision: 1.1 $
############################################################################
#
    #
    #...Input parameters
    #
input_file=$1
dir=$2
    #
    #...Start main
    #
cp ~/bin/LONG_epar.sh ./
file=`LONG_Make_setup.sh ${input_file} flatn`
    #
grating=`echo ${file}|awk -F '_' '{print $2}'`
grangle=`echo ${file}|awk -F '_' '{print $3}'`
gain=`echo ${file}|awk -F '_' '{print $5}'`
read=`echo ${file}|awk -F '_' '{print $6}'`
bin=`echo ${file}|awk -F '_' '{print $7}'`
jd=`echo ${file}|awk -F '_' '{print $8}'`
list=`ls ${dir}|fgrep ${gain}|fgrep ${read}|fgrep ${bin}|fgrep ${grating}|fgrep ${grangle}`
    #
dmax=100000.0
for i in `echo ${list}`
do
    time=`echo ${i}|awk -F '_' '{print $8}'|rpl '.fits' ""`
    dt=`echo ${time} ${jd} ${dmax} ${i}|awk '{dif=sqrt(($2-$1)*($2-$1));if (dif<$3){print dif, $4} else {print "NONE"}}'`
    if [ ! "${dt}" = "NONE" ]; then
	dmax=`echo ${dt}|awk '{print $1}'`
	name=`echo ${dt}|awk '{print $2}'`
    fi
done
rm -f ./LONG_epar.sh
if [ "z${name}" != "z" ]; then
    echo ${name}
else
    echo "NONE"
fi
