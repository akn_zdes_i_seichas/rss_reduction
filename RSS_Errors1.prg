!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2019 SAAO, Cape Town
!.IDENT    : RSS_Errors.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Errors
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Take 1d spectrum and its Errors and insert both into output
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Errors Input_Spec_1df Errors_1d Output
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_Errors.prg,v 1.2 2019/03/23 09:50:22 akniazev Exp akniazev $
!          : $Revision: 1.2 $
!-------------------------------------------------------------------------
!
   define/par p1 ?       I "Input_spectrum:"
   define/par p2 ?       I "Input_errors_for_spectrum:"
   define/par p3 ?       I "Output_spectrum_with_errors:"
	!
	!...Define our local variables
	!
    define/local inamef/c/1/80  "{p1}"
    define/local inames/c/1/80  "{p2}"
    define/local oname/c/1/80   "{p3}"
	!
	!...Main body
	!
	    !...Compute ratio between these spectra and calculate error
	    !
    comp/ima Errorf = {inames}*1.
	    !
	    !...Create 2D frame and put spectrum_f and its error there
	    !
    crea/ima a 2,{{inamef},NPIX},2 {{inamef},START},1.,{{inamef},STEP},1.
    insert/ima {inamef} a <,@1
    insert/ima Errorf   a <,@2
	    !
	    !...Copy all descriptors and move temporary file into output name
	    !
    copy/dd {inamef} *,3   a ?
    copy/dd {inamef} IDENT a ?
    copy/dd {inamef} CUNIT a ?
    copy/ii a {oname}
	    !
	    !...Create/copy a couple of descriptors for 'fbs' program
	    !
!    @@ HRS/HRS_heliocor {oname}
!    set/format ,f14.6
!    write/desc {oname} ENVMJD/d/1/1 {{oname},MJD-OBS}
    write/desc {oname} ENVMJD/d/1/1 2458017.51335
    write/desc {oname} HEL_COR/d/1/1 0.
	    !
	    !...Write FITS-file
	    !
    outd/fits {oname}.bdf {oname}.fits
    set/format
	!
	!...End
	!
    -delete ratio.bdf Errorf.bdf a.bdf
