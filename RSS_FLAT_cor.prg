!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2011 SAAO, Cape Town
!.IDENT    : RSS_Bad_cor.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_FLAT_cor
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Correction for the pixel-to-pixel sensitivity in RSS spectra
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_FLAT_cor MIDAS_catalogue
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_FLAT_cor.prg,v 1.2 2019/06/22 18:30:14 akniazev Exp akniazev $
!          : $Revision: 1.2 $
!-------------------------------------------------------------------------
!
    DEFINE/PAR P1 ? C "Enter_Catalogue_File_Name:"
!
    DEFINE/LOCAL J/I/1/2 0,0
    DEFINE/LOCAL CATAL/I/1/1  0
    DEFINE/LOCAL INPUT/C/1/80 "" ALL
    define/local oname/c/1/60 "IMAGE_TMP"
!    DEFINE/LOCAL OUTPUT/C/1/80 "" ALL
    DEFINE/LOCAL REF/C/1/4 "    "
    DEFINE/LOCAL REF_SP/C/1/80 "" ALL
    DEFINE/LOCAL INCAT/C/1/20 "                    "
    DEFINE/LOCAL II/I/1/2 0,0
    DEFINE/LOCAL I/I/1/2 0,0
    DEFINE/LOCAL flatname/c/1/60 " "
    CLEAR/ICAT
	!
	!...test, if really catalog name was entered...
	!
    write/out "*********************************"
    write/out "***** FLAT FIELD correction *****"
    write/out "*********************************"
	    !
    SHOW/ICAT {P1}
    II(2) = {OUTPUTI(1)}
    II(1) = M$INDEX(P1,".cat")
    IF II(1) .LE. 0 THEN
       WRITE/KEY INCAT {P1}.cat
    ELSE
       WRITE/KEY INCAT {P1}
    ENDIF
	!
	!...Main loop
	!
    J(1) = 0
LOOP:
    STORE/FRAME INPUT {INCAT}
!    READ/KEY INPUT
!    WRITE/OUT input frame {INPUT}, input cat {INCAT}
!           ...
    J(1) = {J(1)} + 1
    write/out "***** {INPUT}: *****"
	!
	!...Will find the nearest normalised FLAT and copy it
	!   into the currect directory. Program will select the name
	!   taking into account grating, angle, gain, read-out and binning
	!
    outd/fits {INPUT} a.fits
    $LONG_RSS_flatn.sh a.fits {dir_flatn} | write/key flatname
    write/out "*****************************************************"
    if "{flatname}" .ne. "NONE" then
        cp -v {dir_flatn}/{flatname} ./
            !
            !...Make flat-field correction
            !
        write/out "***** {INPUT}: {{INPUT},GAINSET}+{{INPUT},ROSPEED} *****"
        if "{{INPUT},GAINSET}" .EQ. "FAINT" .AND. "{{INPUT},ROSPEED}" .EQ. "SLOW" then
            !
            if "{{INPUT},CCDSUM}" .EQ. "2 2" then
            write/out "***** {INPUT}: {{INPUT},CCDSUM} FLAT CORRECTION *****"
            comp/ima {INPUT} = {INPUT}/{flatname}
            endif
            if "{{INPUT},CCDSUM}" .EQ. "2 4" then
            write/out "***** {INPUT}: {{INPUT},CCDSUM} FLAT CORRECTION *****"
            comp/ima {INPUT} = {INPUT}/{flatname}
            endif
            if "{{INPUT},CCDSUM}" .EQ. "4 4" then
            comp/ima {INPUT} = {INPUT}/{flatname}
            endif
        else
            write/out "***** {INPUT}: {{INPUT},GAINSET}+{{INPUT},ROSPEED} FLAT CORRECTION *****"
            comp/ima {INPUT} = {INPUT}/FLATn.fits
        endif
    else
        write/out "There is NO flat in the database for this configuration, sorry ..."
    endif
    write/out "*****************************************************"

    IF CATAL .LT. II(2) GOTO LOOP
END:
    -delete {oname}.bdf a.fits
