!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2016 SAAO
!.IDENT    : RSS_fcam.prg
!.AUTHOR   : A.Yu. Kniazev
!.CALL     : RSS_fcam.prg
!.CALL FROM:
!.KEYWORDS : RSS observations
!.OUTPUT   :
!.PURPOSE  :
!      calculation of the focus value for RSS camera
!.USAGE    :
!      Execute as:
!
!.VERSION  : $Header: /data/midas/RAC/Obsccd/proc/RCS/occd_fcalc_pf500.prg,v 1.1 1996/01/14 18:26:40 midas Exp $
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
    define/par p1 ?         N "Input_Night:"
    define/par p2 1         N "The First number:"
    define/par p3 9         N "The Last number:"
    define/par p4 700,100   N "First point and step for the focus:"
    define/par p5 a         C "Add or new table with lines [A/N]:"
    define/par p6 2         N "Polynom power:"
    define/par p7 5         N "Image to create table:"
	!
	!...Define our local variables
	!
    define/local night/c/1/8  "{p1}"
    define/local fnum/i/1/1    {p2}
    define/local lnum/i/1/1    {p3}
    define/local aa/r/1/2      {p4}
    define/local first/r/1/1   {aa(1)}
    define/local sfoc/r/1/1    {aa(2)}
    define/local iname/c/1/60   " "
    define/local new/c/1/1    "{p5}"
    define/local power/i/1/1   {p6}
    define/local aaa/i/1/1     {p7}
    define/local tnum/i/1/1     0
    define/local yes/c/1/3     "on"
    define/local i/i/1/4        0
    define/local ii/i/1/3       0
    define/local fstart/r/1/1   0
    define/local fstep/r/1/1    0.
    define/local fmin/r/1/1     0.
    define/local fwhm/r/1/1     0.
    define/local limits/r/1/2   0.,0.
    define/local lcount/i/1/1   0
	!
	!...Main body
	!
begin:
	    !
	    !...1.Load image
	    !
    iname = "mrpP{night}{aaa}.fits"
    load/ima {iname} 0 scale=-3,1 cuts=D,3sigma
	    !
	    !...2.Mark work region (for averaging)
	    !
    write/out "----- Please, move image-display up:"
    write/out "----- You need mark down and up row for averaging"
    inq/key yes "----- and press <Enter> when ready"
rep:
    clear/chan over
    set/cursor ? c_hair
    get/cursor tmp
    if {tmp.tbl,TBLCONTR(4)} .lt. 2 then
	write/out "----- WARNING: you need 2 points... Repeate, please..."
	goto rep
    endif
    if {tmp.tbl,Y_COORD,1} .lt. {tmp.tbl,Y_COORD,2} then
	limits(1) = {tmp.tbl,Y_COORD,1}
	limits(2) = {tmp.tbl,Y_COORD,2}
    else
	limits(1) = {tmp.tbl,Y_COORD,2}
	limits(2) = {tmp.tbl,Y_COORD,1}
    endif
	    !
	    !...3.Plot spectrum with best cuts and select lines in the loop
	    !
    aver/row tmpr = {iname} {limits(1)},{limits(2)}
    del/des tmpr lhcuts
    stat/ima tmpr >Null
lines:
    @@ plot_akn tmpr
    write/out
    write/out "----- Please, select lines use cursor (mark edges for each line)"
    write/out
    if lcount .ne. 0 then
	center/gauss GCURSOR list_lines EMISSION a
    else
	if "{new}" .eq. "n" then
	    center/gauss GCURSOR list_lines EMISSION
	else
	    name/col     list_lines #1 :START
	    name/col     list_lines #2 :END
	    center/gauss GCURSOR list_lines EMISSION a
	endif
    endif
    lcount = lcount + 1
    inq/key yes "Some more lines? [Y/N]:"
    if yes(1:1) .eq. "y" goto lines
    name/col     list_lines #1 :XSTART
    name/col     list_lines #2 :XEND
    create/colum list_lines :focus  F7.3 R*4
    sort/tab     list_lines :XSTART
	    !
	    !...4. CENTER/GAUSS for each line for all frames
	    !
		!...Create table for each line
		!
    i(4) = {list_lines.tbl,TBLCONTR(4)}
    i(3) = {lnum} - {fnum} + 1
    do i(2) = 1 {i(4)} 1
	crea/tab lin{i(2)} 3 {i(3)}
	create/colum lin{i(2)} :focus  F7.3 R*4
	create/colum lin{i(2)} :fwhm   F7.3 R*4
	create/colum lin{i(2)} :center F7.3 R*4
	create/colum lin{i(2)} :sig    F7.3 R*4
	create/colum lin{i(2)} :icent  F7.3 R*4
    enddo
		!
		!...For each image CENTER/GAUSS for all lines
		!
    set/format i4
    ii = {fnum}
    do i(1) = 1 {i(3)} 1
	iname = "mrpP{night}{ii}.fits"
	aver/row tmpr = {iname} {limits(1)},{limits(2)}
	center/gauss tmpr,list_lines focus EMISSION
	do i(2) = 1 {i(4)} 1
	    lin{i(2)}.tbl,:fwhm,{i(1)}    = {focus.tbl,:xfwhm,{i(2)}}
	    lin{i(2)}.tbl,:center,{i(1)}  = {focus.tbl,:xcen,{i(2)}}
	    lin{i(2)}.tbl,:sig,{i(1)}     = {focus.tbl,:xsig,{i(2)}}
	    lin{i(2)}.tbl,:icent,{i(1)}   = {focus.tbl,:ICENT,{i(2)}}
	    lin{i(2)}.tbl,:focus,{i(1)}   = {first} + ({i(1)} - 1)*{sfoc}
	enddo
	ii = ii + 1
    enddo
	    !
	    !...5.Compute regressions
	    !
    do i(2) = 1 {i(4)} 1
power:
		!
		!...a.Plot initial data
		!
	set/graph color=1 ltype=0 stype=5
	plot/tab lin{i(2)} :focus :fwhm
	set/graph color=1 ltype=1 stype=0
	over/tab lin{i(2)} :focus :fwhm
		!
		!...b.Compute regression
		!
	regression/polynomial lin{i(2)} :fwhm :focus {power}
	save/regression lin{i(2)} coef
	compute/regression lin{i(2)} :fit = coef
		!
		!...c.Find focus value for minimum
		!
	stat/tab lin{i(2)} :focus
	fstep = (outputr(2)-outputr(1))/500.
	create/ima ref 1,500 {outputr(1)},{fstep} nodata
	convert/table focus = lin{i(2)} :focus :fit ref SPLINE
	stat/image focus option=fn
	set/grap color=2 ltype=1 stype=0
	over/row focus
	fmin = {focus,start(1)} + {focus,step(1)}*({outputi(2)} - 1)
	set/format f6.3
	write/out "Focus value for minimum fwhm = {fmin} pixels"
	write/out
		!
		!...d.Do you want to repeat?
		!
	inq/key yes "Do you want repeat with new power [y/n]: "
	if yes(1:1) .eq. "y" then
	    inq/key power/i/1/1  "Input new power value [was = {power}]:"
	    goto power
	endif
	list_lines.tbl,:focus,{i(2)} = {fmin}
    enddo
	    !
	    !...6.Plot final results
	    !
	set/graph color=1 ltype=0 stype=5
	plot/tab list_lines :center :focus
	set/graph color=1 ltype=1 stype=0
	over/tab list_lines :center :focus
	    !
	    !...7.Return some initial values
	    !
end:
	set/graph
	set/graph pmode=1 font=1
