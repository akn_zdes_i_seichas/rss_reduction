!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2006 SAAO, Cape Town
!.IDENT    : Extract.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ Extract
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      SALT reduction. Some additional steps.
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ Extract Night_name
! where:
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/RSS_Extract.prg,v 1.5 2019/06/24 18:00:43 akniazev Exp akniazev $
!          : $Revision: 1.5 $
!-------------------------------------------------------------------------
!
    define/param p1 ?      C "Input night [mbpxP20060425]:"
    define/param p2 ?      C "Input CCD part [<,<:>,>]:"
    define/param p3 OBJECT C "Reduction Type: (OBJECT/GAIN/FLAT)"
	!
	!...Define local variables
	!
    define/local otype/c/1/20     "{p3}"
    define/local n/i/1/1           0
    define/local ilim/i/1/2        0,0
    define/local flatname/c/1/60  " "
    @@ RSS_Global
	!
	!...Main body
	!
    -delete *.bdf {p1}*ee.fits
    $ICAT_crea.sh {p1}????.fits >IN.cat
    $rpl "\.fits" "" <IN.cat >OUT.cat
    exec/cat indis/mfits IN.cat OUT.cat ? ? ? copy,*
    crea/icat HEAD0 {p1}????0000.bdf
    crea/icat OBJ   {p1}????0001.bdf
    exec/cat  copy/dd HEAD0.cat IDENT,O_POS,O_TIME OBJ.cat
    -delete {p1}????0000.bdf
    exec/cat write/des OBJ.cat start 1.,1.
    exec/cat write/des OBJ.cat step  1.,1.
    if "{otype}" .ne. "GAIN" then
        @@ RSS_Gain_cor OBJ.cat
        if "{otype}" .ne. "FLAT" then
            @@ RSS_FLAT_cor OBJ.cat
        endif
    endif
    @@ RSS_Bad_cor  OBJ.cat
    if "{otype}" .ne. "GAIN" then
        @@ RSS_Gaps_cor OBJ.cat
    endif
    $rpl "0001\.bdf" "e.bdf" <OBJ.cat >OBJe.cat
    exec/cat extra/ima OBJe.cat = OBJ.cat {p2}
    exec/cat write/des OBJe.cat start 1.,1.
    if "{otype}" .eq. "OBJECT" then
        @@ ldss_init1
        del/dis 0
        del/gra 0
        crea/dis 0 1200,900
        @@ RSS_Cosmic_cor OBJe.cat
    else
        $rpl  "e.bdf" "ee.bdf" <OBJe.cat >OBJee.cat
        exec/cat copy/ii OBJe.cat OBJee.cat
    endif
    $ls {p1}*ee.bdf >in.cat
    $rpl "\.bdf" ".fits" <in.cat >out.cat
    outd/fits in.cat out.cat
    $rm -vf {p1}*.bdf
    if "{otype}" .eq. "GAIN" then
        $rm -f FLAT.fits
        $LONG_flat.sh
            !
            !...Try to calculate the range for gain calculation
            !
        n = {FLAT.fits,NPIX(2)}
        ilim(1) = {n}/4
        ilim(2) = {n}
        @@ RSS_Gain_cor_calc FLAT.fits @{ilim(1)},@{ilim(2)} Gain_cor
            !
            !...Form name and copy to the database directory
            !
        $LONG_Make_setup.sh FLAT.fits gain | write/key flatname
            !
            !...DO NOT REMOVE Gain_cor.tbl table!!!
            !   It is used by oher scripts!!!
            !
        cp Gain_cor.tbl {flatname}.tbl
        cp {flatname}.tbl {dir_gain}
            !
    elseif "{otype}" .eq. "FLAT" then
        -delete  FLAT.fits
        $LONG_flat.sh
        @@ RSS_FLAT_norm_cre FLAT.fits FLAT_n.fits 1.,1.,0.0
            !
            !...Form names and copy to the database directory
            !
        $LONG_Make_setup.sh FLAT.fits flat | write/key flatname
        mv FLAT.fits {flatname}.fits
        cp {flatname}.fits {dir_flat}
        $LONG_Make_setup.sh FLAT_n.fits flatn | write/key flatname
        mv FLAT_n.fits {flatname}.fits
        cp {flatname}.fits {dir_flatn}
    endif
	!
	!...The End
	!
    -delete IN.cat OBJ.cat OBJe.cat OBJee.cat OUT.cat HEAD0.cat in.cat out.cat
    -delete cosmask.bdf middumma.bdf dirfile.ascii
    -delete i__i.cat r__r.cat outnames.cat middummclear.prg Mid*Pipe
    -delete List.txt
