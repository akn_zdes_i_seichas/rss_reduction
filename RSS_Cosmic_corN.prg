!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT: (c) 2020 SAAO, Cape Town
!.IDENT    : RSS_Cosmic_corN.prg
!.AUTHOR   : A.Y. Kniazev
!.CALL     : @@ RSS_Cosmic_corN
!.CALL FROM:
!.KEYWORDS :
!.OUTPUT   :
!.PURPOSE  :
!      Correction for cosmic in RSS spectra
!
!.COMMENTS :
!.USAGE    :
!      Execute as:
! @@ RSS_Bad_cor
! where:
!
!
!.VERSION  : $Header: /home/akniazev/midwork/RCS/ldss_ContCre.prg,v 1.1 2003/04/05 10:42:30 akniazev Exp akniazev $
!          : $Revision: 1.1 $
!-------------------------------------------------------------------------
!
DEFINE/PAR P1 ? C "Enter_Input_File_Name:"
DEFINE/PAR P2 ? C "Enter_Output_File_Name:"
DEFINE/PAR P3 ? C "Enter_Mask_File_Name:"
DEFINE/PAR P4 ? N "Number_of_additional_iterations:"
!
DEFINE/LOCAL input/C/1/80  "{P1}"
DEFINE/LOCAL output/C/1/80 "{P2}"
DEFINE/LOCAL mask/C/1/80   "{P3}"
DEFINE/LOCAL iter/i/1/1     {P4}
DEFINE/LOCAL scale/C/1/5   "-1,-1"
!
!...Main body
!
stat/ima {INPUT} OPTION=W !>Null
if {outputr(8)} .le. 12 then
    par(2) = 4.0
    par(3) = 4.0
elseif {outputr(8)} .gt. 12 .and. {outputr(8)} .le. 50 then
    par(2) = 4.0
    par(3) = 7.0
elseif {outputr(8)} .gt. 50 .and. {outputr(8)} .le. 100 then
    par(2) = 3.7
    par(3) = 6.0
!    par(2) = 2.8
!    par(3) = 5.3
elseif {outputr(8)} .gt. 100 .and. {outputr(8)} .le. 150 then
    par(2) = 3.7
    par(3) = 6.0
!    par(3) = 5.3
!    par(2) = 2.3
!    par(3) = 4.5
elseif {outputr(8)} .gt. 150 .and. {outputr(8)} .le. 200 then
!    par(2) = 3.1
!    par(3) = 5.1
    par(2) = 2.5
    par(3) = 4.5
elseif {outputr(8)} .gt. 200 .and. {outputr(8)} .le. 300 then
!    par(2) = 4.5
!    par(3) = 6.4
    par(2) = 3.3
    par(3) = 5.4
!    par(2) = 1.0
!    par(3) = 1.5
elseif {outputr(8)} .gt. 300 .and. {outputr(8)} .le. 500 then
    par(2) = 3.5
    par(3) = 5.4
elseif {outputr(8)} .gt. 500 .and. {outputr(8)} .le. 1000 then
    par(2) = 3.5
    par(3) = 6.0
elseif {outputr(8)} .gt. 1000 .and. {outputr(8)} .le. 2000 then
    par(2) = 3.5
    par(3) = 6.5
elseif {outputr(8)} .gt. 2000 then
    par(2) = 3.0
    par(3) = 5.5
else
    par(2) = 3.0
    par(3) = 5.5
endif
if "{{INPUT},GAINSET}" .EQ. "BRIGHT" .AND. "{{INPUT},ROSPEED}" .EQ. "FAST" then
    par(2) = {par(2)} + 1.
    par(3) = {par(3)} + 1.
endif
if "{{INPUT},CCDSUM}" .EQ. "2 4" then
    par(2) = {par(2)} * 1.2
    par(3) = {par(3)} * 1.2
endif
if "{{INPUT},PROPID}" .eq. "CAL_SPST" then
    par(2) = {par(2)} + 20
    par(3) = {par(3)} * 3
endif
del/des {input} lhcuts
load/ima  {input} 0 scale={scale} cuts=D,3sigma
@@ RSS_Cosmic_Lap {input} {output} {smo(1)},{smo(2)} {par(1)},{par(2)},{par(3)},{par(4)},{par(5)},{iter} {mask} 1.,2.5 N ?
load/ima  {mask} scale={scale}
wait 1
load/ima  {output} 0 scale={scale} cuts=D,3sigma
wait 1
